/************************************************************************************
����:  VertexFormats.h
�����: ������� �������

�������� ��������������� ������� ������ � �� ���������.
************************************************************************************/

#ifndef __VertexFormatsH__
#define __VertexFormatsH__

#include <D3DX11.h>
#include <D3DX10.h>

namespace ServiceTools
{
	struct ColorVertex
	{
		D3DXVECTOR3 m_position;
		D3DXVECTOR4 m_color;
	};
	
	static D3D11_INPUT_ELEMENT_DESC pColorVertexDescription[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};

	///////////////////////////////////////////////////////////////////////////////////////////

	struct TextureVertex
	{
		D3DXVECTOR3 m_position;
		D3DXVECTOR3 m_normal;
		D3DXVECTOR2 m_texel;
	};

	static D3D11_INPUT_ELEMENT_DESC pTextureVertexDescription[]=
	{
		{"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0},
		{"NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0},
		{"TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0} 
	};

	///////////////////////////////////////////////////////////////////////////////////////////

	/*struct BumpVertex {
		D3DXVECTOR3 m_position;
		D3DXVECTOR3 m_normal;
		D3DXVECTOR3 m_tangent;
		D3DXVECTOR3 m_bitangent;
		D3DXVECTOR2 m_texCoord;

		BumpVertex()
		{
			m_position = { 0.0f, 0.0f, 0.0f };
			m_normal = { 0.0f, 0.0f, 0.0f };
			m_tangent = { 0.0f, 0.0f, 0.0f };
			m_bitangent = { 0.0f, 0.0f, 0.0f };
			m_texCoord = { 0.0f, 0.0f };
		}
	};

	static D3D11_INPUT_ELEMENT_DESC g_bumpVertexDesc[] = 
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "BINORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 36, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 48, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};*/

	struct BumpVertex {
		D3DXVECTOR3 m_position;
		D3DXVECTOR3 m_normal;
		D3DXVECTOR4 m_tangent;
		D3DXVECTOR2 m_texCoord;

		BumpVertex()
		{
			m_position = { 0.0f, 0.0f, 0.0f };
			m_normal = { 0.0f, 0.0f, 0.0f };
			m_tangent = { 0.0f, 0.0f, 0.0f, 0.0f };
			m_texCoord = { 0.0f, 0.0f };
		}
	};

	static D3D11_INPUT_ELEMENT_DESC g_bumpVertexDesc[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TANGENT", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 40, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};

	////////////////////////////////////////////////////////////////////////////////////////////
}

#endif//__VertexFormatsH__