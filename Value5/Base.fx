/************************************************************************************
����:  Base.fx
�����: ������� �������

�������� ��������, ���������� �������������� � ����������� �����.
************************************************************************************/

#ifndef __BaseFX__
#define __BaseFX__

#include "LightingHelper.fx"

Texture2D g_objectTexture;
TextureCube g_skyCubemap;
Texture2D g_normalMap;

cbuffer PerFrameBase
{
	float4x4 g_worldMatrix;
	float4x4 g_normalCorrectionMatrix;
	float4x4 g_textureTransformMatrix;
	float4x4 g_viewMatrix;
	float3   g_viewerPositionW;					
	PointLightSource g_pointLightSource;
	DirectionalLightSource g_directionalLightSource;
	SpotLightSource g_spotLightSource;
};

cbuffer PerObjectBase
{
	Material g_objectMaterial;
};

cbuffer PerCenturyBase
{
	float4x4 g_projectionMatrix;
	bool g_toUseEnvironmentMapping;
	float g_distanceWithMaxTess;
	float g_distanceWithMinTess;
	float g_maxTessFactor;
	float g_minTessFactor;
	float g_heightScale;
};

#endif//__BaseFX__