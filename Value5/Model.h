#pragma once

#include <vector>
#include <string>
#include "Mesh.h"

namespace GraphicsSystem
{
	class Model {
	private:
		std::vector<GraphicsSystem::Mesh*> m_meshes;
	public:
		Model(const std::string& model_filename);

		void draw(DRAW_MODE_FLAGS flags = DRAW_MODE_FLAGS::NONE);
		void update(float delta_time);

		void setWorldMatrix(D3DXMATRIX& world_matrix);

		~Model();
	};
}