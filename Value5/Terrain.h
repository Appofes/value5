#pragma once

#include <iostream>
#include <string>
#include <vector>
#include "SimpleGraphicsObject.h"

namespace GraphicsSystem
{
	const int NUM_COLOR_MAPS = 5;
	const int NUM_CELLS_PER_PATCH = 64;

	struct TerrainVertex {
		D3DXVECTOR3 m_position;
		D3DXVECTOR2 m_texCoord;
		D3DXVECTOR2 m_boundsY;
	};

	const D3D11_INPUT_ELEMENT_DESC TERRAIN_VERTEX_DESC[] = 
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD0", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD1", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 20, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};

	class Terrain : public SimpleGraphicsObject {
	private:
		std::wstring m_heightMapFilename;
		std::wstring m_colorMapFilenames[NUM_COLOR_MAPS];
		std::wstring m_blendMapFilename;
		std::vector<float> m_heightMap;
		std::vector<D3DXVECTOR2> m_patchBoundsY;
		float m_heightScale;
		int m_heightMapWidth;
		int m_heightMapHeight;
		float m_cellSpacing;
		ID3D11ShaderResourceView* m_colorMapsArraySRV;
		ID3D11ShaderResourceView* m_heightMapSRV;
		ID3D11ShaderResourceView* m_blendMapSRV;
	public:
		Terrain(const std::wstring& heightmap_filename,
			const std::wstring colormap_filenames[NUM_COLOR_MAPS],
			const std::wstring& blendmap_filename,
			float height_scale,
			int heightmap_width,
			int heightmap_height,
			float cell_spacing);

		void loadHeightmap(const std::wstring& heightmap_filename);
	};
}