/************************************************************************************
����:  Materials.h
�����: ������� �������

�������� ��������������� ��������� ��������� �������.
************************************************************************************/

#ifndef __MaterialsH__
#define __MaterialsH__

#include "LightingHelper.h"
#include "Colors.h"
#include <D3DX10.h>
using namespace GraphicsSystem;
using namespace ServiceTools;

namespace ServiceTools
{
	const Material<D3DXVECTOR4> WHITE_MATERIAL_ROUGH = { WHITE_COLOR, BLACK_COLOR, DEFAULT_SPECULAR_EXPONENT_AS_VECTOR, NO_REFLECTION };
	const Material<D3DXVECTOR4> WHITE_MATERIAL_SHINY = { WHITE_COLOR, WHITE_COLOR, DEFAULT_SPECULAR_EXPONENT_AS_VECTOR, NO_REFLECTION };
}

#endif//__MaterialsH__