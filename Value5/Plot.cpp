#include "Plot.h"
using namespace GraphicsSystem;

Plot::Plot(D3DXVECTOR2* p_points, int number_of_points, float line_thickness)
{
	m_pVertexBuffer = 0;
	m_pInputLayout = nullptr;
	m_pEffect = nullptr;
	m_pTechnique = nullptr;
	m_vertexStride = sizeof(Vertex);
	m_vertexOffset = 0;
	D3DXMatrixIdentity(&m_worldMatrix);
	m_numberOfPoints = number_of_points;

	D3D11_BUFFER_DESC vertexBufferDesc;
	vertexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	vertexBufferDesc.ByteWidth = sizeof(Help::Vertex) * number_of_points;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA vertexBufferInitData;
	vertexBufferInitData.pSysMem = p_points; //DANGEROUS

	DEVICE->CreateBuffer(&vertexBufferDesc, &vertexBufferInitData, &m_pVertexBuffer);

	CreateEffect("Graph.fxo", &m_pEffect);
	InitializeEffect(m_worldMatrix, CORE.getViewMatrix(), CORE.getProjectionMatrix(), m_lineThickness, m_pEffect);

	m_pTechnique = m_pEffect->GetTechniqueByName("ValueGraph");
	m_pTechnique->GetPassByIndex(0)->Apply(0, CONTEXT);

	D3DX11_PASS_DESC passDesc;
	m_pTechnique->GetPassByIndex(0)->GetDesc(&passDesc);

	DEVICE->CreateInputLayout(Help::pVertexDescription, 1, passDesc.pIAInputSignature, passDesc.IAInputSignatureSize, &m_pInputLayout);
}