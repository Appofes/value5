/************************************************************************************
����:  SamplerStates.fx
�����: ������� �������

�������� ��������������� ������ ������� �� �������� � ���������.
************************************************************************************/

#ifndef __SamplerStatesFX__
#define __SamplerStatesFX__

SamplerState LinearWrap
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = WRAP;
	AddressV = WRAP;
};

#endif//__SamplerStatesFX__