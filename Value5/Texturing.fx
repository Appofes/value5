#include "LightingHelper.fx"

Texture2D g_texture;

//�Ѩ ����� ������������
//������ ������� � ��������� ���������� ��������� ��������� ������ � ������������ ���������
//������������ ����� �������� ����������� �������� �������

SamplerState effectSampler
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = WRAP;
	AddressW = WRAP;
};

cbuffer cbPerObject
{
	float4x4 g_worldViewProj;
	float4x4 g_normalCorrectionTransform;
};

struct VertexIn
{
	float3 m_positionL  : POSITION;
	float3 m_normalL	: NORMAL;
	float2 m_texel		: TEXCOORD;
};

struct VertexOut
{
	float4 m_positionH  : SV_POSITION;
	float3 m_positionL  : POSITION;
	float3 m_normalW	: NORMAL;
	float2 m_texel		: TEXCOORD;
};

VertexOut VS(VertexIn in_vertex)
{
	VertexOut outVertex;

	outVertex.m_positionH = mul(float4(in_vertex.m_positionL, 1.0f), g_worldViewProj);
	outVertex.m_positionL = in_vertex.m_positionL;
	outVertex.m_normalW = mul(float4(in_vertex.m_normalL, 0.0f), g_normalCorrectionTransform);
	outVertex.m_texel = in_vertex.m_texel;

	return outVertex;
}

float4 PS(VertexOut pin) : SV_Target
{
	float4 texelColor = g_texture.Sample(effectSampler, pin.m_texel);
	return texelColor;	
}

technique11 Texturing
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_5_0, VS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_5_0, PS()));
	}
}
