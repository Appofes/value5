/************************************************************************************
����:  Quad.h
�����: Quad
�����: ������� �������

�����, ����������� ��������� SimpleGraphicsObject, �������������� ��������� ������-
���������.
************************************************************************************/

#ifndef __QuadH__
#define __QuadH__

#include "SimpleGraphicsObject.h"
using namespace GraphicsSystem;

namespace GraphicsSystem
{
	class Quad : public SimpleGraphicsObject
	{
	private:
		DRAW_MODE_FLAGS m_flags;
	public:
		Quad(LPCWSTR texture_file_name);
		void draw(DRAW_MODE_FLAGS flags = DRAW_MODE_FLAGS::NONE);
		void update(float delta_time);
		void setWorldMatrix(D3DXMATRIX& world_matrix);
		void setTextureTransformMatrix(D3DXMATRIX& texture_transform_matrix);
		D3DXMATRIX worldMatrix();
		~Quad();
	};
}


#endif//__QuadH__