#pragma once

#include "SimpleGraphicsObject.h"
#include <string>

namespace GraphicsSystem
{
	const int STACK_COUNT = 30;
	const int SLICE_COUNT = 30;
	const float RADIUS = 5000.0f;

	class Sky : public SimpleGraphicsObject {
	private:
		int m_numIndices;
		ID3D11ShaderResourceView* m_cubemapSRV;
	public:
		Sky(const std::string& cubemap_filename);
		~Sky();

		void draw(DRAW_MODE_FLAGS flags = DRAW_MODE_FLAGS::NONE);
		void update(float delta_time);

		void setWorldMatrix(D3DXMATRIX& world_matrix);
		void setTextureTransformMatrix(D3DXMATRIX& texture_transform_matrix);

		D3DXMATRIX worldMatrix();

		ID3D11ShaderResourceView* cubemapSRV();
	};
}