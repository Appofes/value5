/************************************************************************************
����:  Quad.cpp
�����: Quad
�����: ������� �������

����������� �������, ���������� �� ������������� � ��������� �������, � ����� ��� ��-
����������.
************************************************************************************/

#include "Quad.h"
#include "Base.h"
#include "Macros.h"
#include "Materials.h"
#include "VertexFormats.h"
#include "EffectHelper.h"

Quad::Quad(LPCWSTR p_texture_file_name)
{
	m_flags = DRAW_MODE_FLAGS::NONE;//

	m_pVertexBuffer = 0;
	m_pIndexBuffer = 0;
	m_pInputLayout = 0;
	m_pEffect = 0;
	m_pTechnique = 0;
	m_vertexStride = 0;
	m_vertexOffset = 0;
	m_indexOffset = 0;
	m_material = WHITE_MATERIAL_SHINY;
	m_material.m_reflectionColor = { 0.5f, 0.5f, 0.5f, 1.0f };
	m_vertexStride = sizeof(TextureVertex);
	D3DXMatrixIdentity(&m_worldMatrix);
	D3DXMatrixIdentity(&m_textureTransformMatrix);

	InitializeEffect("LightingModel.fxo", &m_pEffect);
	UpdateEffectBase(&m_worldMatrix, &m_textureTransformMatrix, &m_material, p_texture_file_name, m_pEffect);

	m_pTechnique = m_pEffect->GetTechniqueByName("Lighting");
	m_pTechnique->GetPassByIndex(0)->Apply(0, BASE.getContextPointer());

	D3DX11_PASS_DESC passDesc;
	m_pTechnique->GetPassByIndex(0)->GetDesc(&passDesc);
	HR(BASE.getDevicePointer()->CreateInputLayout(pTextureVertexDescription, 3, passDesc.pIAInputSignature,
		passDesc.IAInputSignatureSize, &m_pInputLayout));

	float width  = 1.0f;
	float height = 1.0f;
	float depth  = 0.0f;

	TextureVertex pVertices[6];

	INITIALIZING_VERTEX_BUFFER
	{
		pVertices[0] = { D3DXVECTOR3(-width, -height, -depth), D3DXVECTOR3(0.0f, 0.0f, -1.0f), D3DXVECTOR2(0.0f, 1.0f) };
		pVertices[1] = { D3DXVECTOR3(-width, +height, -depth), D3DXVECTOR3(0.0f, 0.0f, -1.0f), D3DXVECTOR2(0.0f, 0.0f) };
		pVertices[2] = { D3DXVECTOR3(+width, +height, -depth), D3DXVECTOR3(0.0f, 0.0f, -1.0f), D3DXVECTOR2(1.0f, 0.0f) };
		pVertices[3] = { D3DXVECTOR3(-width, -height, -depth), D3DXVECTOR3(0.0f, 0.0f, -1.0f), D3DXVECTOR2(0.0f, 1.0f) };
		pVertices[4] = { D3DXVECTOR3(+width, +height, -depth), D3DXVECTOR3(0.0f, 0.0f, -1.0f), D3DXVECTOR2(1.0f, 0.0f) };
		pVertices[5] = { D3DXVECTOR3(+width, -height, +depth), D3DXVECTOR3(0.0f, 0.0f, -1.0f), D3DXVECTOR2(1.0f, 1.0f) };
	}

	D3D11_BUFFER_DESC vertexBufferDescription;
	vertexBufferDescription.Usage = D3D11_USAGE_IMMUTABLE;
	vertexBufferDescription.ByteWidth = sizeof(TextureVertex)* 6;
	vertexBufferDescription.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDescription.CPUAccessFlags = 0;
	vertexBufferDescription.MiscFlags = 0;
	vertexBufferDescription.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA vertexBufferInitData;
	vertexBufferInitData.pSysMem = pVertices;
	HR(BASE.getDevicePointer()->CreateBuffer(&vertexBufferDescription, &vertexBufferInitData, &m_pVertexBuffer));	
}

void Quad::setWorldMatrix(D3DXMATRIX& world_matrix)
{
	m_worldMatrix = world_matrix;
}

void Quad::setTextureTransformMatrix(D3DXMATRIX& texture_transform_matrix)
{
	m_textureTransformMatrix = texture_transform_matrix;
}

D3DXMATRIX Quad::worldMatrix()
{
	return m_worldMatrix;
}

void Quad::update(float delta_time)
{
	UpdateEffectBase(&m_worldMatrix, &m_textureTransformMatrix, 0, 0, m_pEffect);
}

void Quad::draw(DRAW_MODE_FLAGS flags)
{
	if (m_flags != flags)
	{
		m_flags = flags;

		if (flags & DRAW_MODE_FLAGS::USE_ENVIRONMENT_MAPPING)
		{
			auto toUseEnvironmentMapping = m_pEffect->GetVariableByName("g_toUseEnvironmentMapping")->AsScalar();
			toUseEnvironmentMapping->SetBool(true);

			ReleaseCOM(toUseEnvironmentMapping);
		}
		else
		{
			auto toUseEnvironmentMapping = m_pEffect->GetVariableByName("g_toUseEnvironmentMapping")->AsScalar();
			toUseEnvironmentMapping->SetBool(false);

			ReleaseCOM(toUseEnvironmentMapping);
		}
	}

	ID3D11DeviceContext* pContext = BASE.getContextPointer();

	pContext->IASetInputLayout(m_pInputLayout);
	pContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	pContext->IASetVertexBuffers(0, 1, &m_pVertexBuffer, &m_vertexStride, &m_vertexOffset);
	
	D3DX11_TECHNIQUE_DESC techDesc;
	m_pTechnique->GetDesc(&techDesc);
	for (UINT p = 0; p < techDesc.Passes; ++p)
	{
		m_pTechnique->GetPassByIndex(p)->Apply(0, pContext);
		pContext->Draw(6, 0);
	}
}

Quad::~Quad()
{
	ReleaseCOM(m_pVertexBuffer);
	ReleaseCOM(m_pIndexBuffer);
	ReleaseCOM(m_pInputLayout);
	ReleaseCOM(m_pTechnique);
	ReleaseCOM(m_pEffect);
}