/************************************************************************************
����:  MatrixTransforms.h
�����: ������� �������

�������� �������, ����������� ��������� ��������������.
************************************************************************************/

#ifndef __MatrixTransformsH__
#define __MatrixTransformsH__

#include <D3DX11.h>
#include <D3DX10.h>
#include <xnamath.h>
#include <iostream>
#include <vector>
#include "VertexFormats.h"

namespace MathTools
{
	template <typename VectorType, typename MatrixType>
	__forceinline MatrixType ChangeOfCoordinateTransformation(VectorType u_vector, VectorType v_vector, VectorType w_vector, VectorType q_vector)
	{
		MatrixType coordinatesTransform;
		coordinatesTransform._11 = u_vector.x;
		coordinatesTransform._12 = u_vector.y;
		coordinatesTransform._13 = u_vector.z;
		coordinatesTransform._14 = 0.0f;

		coordinatesTransform._21 = v_vector.x;
		coordinatesTransform._22 = v_vector.y;
		coordinatesTransform._23 = v_vector.z;
		coordinatesTransform._24 = 0.0f;

		coordinatesTransform._31 = w_vector.x;
		coordinatesTransform._32 = w_vector.y;
		coordinatesTransform._33 = w_vector.z;
		coordinatesTransform._34 = 0.0f;

		coordinatesTransform._41 = q_vector.x;
		coordinatesTransform._42 = q_vector.y;
		coordinatesTransform._43 = q_vector.z;
		coordinatesTransform._44 = 1.0f;

		return coordinatesTransform;
	}

	__forceinline D3DXMATRIX NormalCorrectionTransformation(D3DXMATRIX& source_transformation)
	{
		D3DXMATRIX normalCorrectionTransform = source_transformation;
		normalCorrectionTransform._41 = 0.0f;
		normalCorrectionTransform._42 = 0.0f;
		normalCorrectionTransform._43 = 0.0f;
		normalCorrectionTransform._44 = 1.0f;

		D3DXMatrixInverse(&normalCorrectionTransform, 0, &normalCorrectionTransform);
		D3DXMatrixTranspose(&normalCorrectionTransform, &normalCorrectionTransform);

		return normalCorrectionTransform;		
	}

	__forceinline D3DXVECTOR4 WorldMatrixToPosition(const D3DXMATRIX& world_matrix)
	{
		D3DXVECTOR4 origin = { 0.0f, 0.0f, 0.0f, 1.0f };
		D3DXVECTOR4 position;

		D3DXVec4Transform(&position, &origin, &world_matrix);

		return position;
	}

	//// It's okay, but bitangets are computed at loading time, it can produce distortion when interpolating across triangle in shaders
	//__forceinline void CalculateTangents(std::vector<ServiceTools::BumpVertex>* vertices, const std::vector<uint32_t>& indices)
	//{
	//	using namespace std;

	//	int numTriangles = indices.size() / 3;

	//	for (int i = 0; i < numTriangles; i++)
	//	{
	//		auto& v0 = vertices->at(indices[i * 3 + 0]);
	//		auto& v1 = vertices->at(indices[i * 3 + 1]);
	//		auto& v2 = vertices->at(indices[i * 3 + 2]);

	//		auto e0 = v1.m_position - v0.m_position;
	//		auto e1 = v2.m_position - v0.m_position;

	//		float s0 = v1.m_texCoord.x - v0.m_texCoord.x;
	//		float s1 = v2.m_texCoord.x - v0.m_texCoord.x;
	//		float t0 = v1.m_texCoord.y - v0.m_texCoord.y;
	//		float t1 = v2.m_texCoord.y - v0.m_texCoord.y;

	//		float r = 1.0f / (s0 * t1 - s1 * t0);

	//		D3DXVECTOR3 sDir;
	//		sDir.x = (t1 * e0.x - t0 * e1.x) * r;
	//		sDir.y = (t1 * e0.y - t0 * e1.y) * r;
	//		sDir.z = (t1 * e0.z - t0 * e1.z) * r;

	//		D3DXVECTOR3 tDir;
	//		tDir.x = (s0 * e1.x - s1 * e0.x) * r;
	//		tDir.y = (s0 * e1.y - s1 * e0.y) * r;
	//		tDir.z = (s0 * e1.z - s1 * e0.z) * r;
	//		
	//		v0.m_tangent += sDir;
	//		v0.m_bitangent += tDir;

	//		v1.m_tangent += sDir;
	//		v1.m_bitangent += tDir;
	//		
	//		v2.m_tangent += sDir;
	//		v2.m_bitangent += tDir;
	//	}

	//	for (int i = 0; i < vertices->size(); i++)
	//	{
	//		auto& vertex = vertices->at(i);

	//		vertex.m_tangent = vertex.m_tangent - vertex.m_normal * D3DXVec3Dot(&vertex.m_normal, &vertex.m_tangent);
	//		D3DXVec3Normalize(&vertex.m_tangent, &vertex.m_tangent);

	//		D3DXVECTOR3 crossResult;

	//		auto dir = (D3DXVec3Dot(D3DXVec3Cross(&crossResult, &vertex.m_normal, &vertex.m_tangent), &vertex.m_bitangent) < 0.0f) 
	//			? -1.0f 
	//			:  1.0f;

	//		D3DXVec3Cross(&vertex.m_bitangent, &vertex.m_normal, &vertex.m_tangent);
	//		vertex.m_bitangent *= dir;
	//	}
	//}

	__forceinline void CalculateTangents(std::vector<ServiceTools::BumpVertex>* vertices, const std::vector<uint32_t>& indices)
	{
		using namespace std;

		vector<D3DXVECTOR3> bitangents(vertices->size());
		memset(bitangents.data(), 0, bitangents.size() * sizeof(D3DXVECTOR3));

		int numTriangles = indices.size() / 3;

		for (int i = 0; i < numTriangles; i++)
		{
			auto& v0 = vertices->at(indices[i * 3 + 0]);
			auto& v1 = vertices->at(indices[i * 3 + 1]);
			auto& v2 = vertices->at(indices[i * 3 + 2]);

			auto e0 = v1.m_position - v0.m_position;
			auto e1 = v2.m_position - v0.m_position;

			float s0 = v1.m_texCoord.x - v0.m_texCoord.x;
			float s1 = v2.m_texCoord.x - v0.m_texCoord.x;
			float t0 = v1.m_texCoord.y - v0.m_texCoord.y;
			float t1 = v2.m_texCoord.y - v0.m_texCoord.y;

			float r = 1.0f / (s0 * t1 - s1 * t0);

			D3DXVECTOR4 sDir;
			sDir.x = (t1 * e0.x - t0 * e1.x) * r;
			sDir.y = (t1 * e0.y - t0 * e1.y) * r;
			sDir.z = (t1 * e0.z - t0 * e1.z) * r;
			sDir.w = 0.0f;

			D3DXVECTOR3 tDir;
			tDir.x = (s0 * e1.x - s1 * e0.x) * r;
			tDir.y = (s0 * e1.y - s1 * e0.y) * r;
			tDir.z = (s0 * e1.z - s1 * e0.z) * r;
			
			v0.m_tangent += sDir;
			bitangents[indices[i * 3 + 0]] += tDir;

			v1.m_tangent += sDir;
			bitangents[indices[i * 3 + 1]] += tDir;
			
			v2.m_tangent += sDir;
			bitangents[indices[i * 3 + 2]] += tDir;
		}

		for (int i = 0; i < vertices->size(); i++)
		{
			D3DXVECTOR3 tangent = vertices->at(i).m_tangent;
			D3DXVECTOR3 bitangent = bitangents[i];
			D3DXVECTOR3 normal = vertices->at(i).m_normal;

			tangent = tangent - normal * D3DXVec3Dot(&normal, &tangent);
			D3DXVec3Normalize(&tangent, &tangent);

			D3DXVECTOR3 crossResult;

			float w = (D3DXVec3Dot(D3DXVec3Cross(&crossResult, &normal, &tangent), &bitangent) < 0.0f) 
				? -1.0f 
				:  1.0f;

			vertices->at(i).m_tangent = D3DXVECTOR4(tangent, w);
		}
	}
}

#endif// __MatrixTransformsH__