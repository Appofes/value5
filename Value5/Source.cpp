/************************************************************************************
����:  Source.cpp
�����: ������� �������

�������� ������� WinMain, � ������� ���������� ���������� ���������.
************************************************************************************/

#include <Windows.h>
#include "Base.h"

int WINAPI WinMain(HINSTANCE hinstance, HINSTANCE prevInstance, PSTR cmdLine, int showCmd)
{
	BASE.initializeWindow(hinstance);
	BASE.initializeDirect3D();
	BASE.initializeDirectInput(hinstance);
	BASE.setup();
	
	return BASE.gameLoop();	
}
