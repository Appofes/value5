TextureCube g_cubeMap;

cbuffer PerFrameConsts {
	float4x4 g_worldViewProj;
};

SamplerState TrilinearSampling {
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = WRAP;
	AddressV = WRAP;
};

RasterizerState NoCulling {
	CullMode = NONE;
};

DepthStencilState LessOrEqualDepthStencilState {
	DepthFunc = LESS_EQUAL;
};

struct VS_INPUT {
	float3 m_positionL : POSITION;
};

struct VS_OUTPUT {
	float4 m_positionH : SV_POSITION;
	float3 m_positionL : POSITION;
};

VS_OUTPUT VS(VS_INPUT vertex)
{
	VS_OUTPUT outVertex;

	outVertex.m_positionH = mul(float4(vertex.m_positionL, 1.0f), g_worldViewProj).xyww;
	
	outVertex.m_positionL = vertex.m_positionL;

	return outVertex;
}

float4 PS(VS_OUTPUT pixel) : SV_TARGET
{
	return g_cubeMap.Sample(TrilinearSampling, pixel.m_positionL);
}

technique11 SkySphere
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_5_0, VS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_5_0, PS()));

		SetRasterizerState(NoCulling);
		SetDepthStencilState(LessOrEqualDepthStencilState, 0);
	}
}