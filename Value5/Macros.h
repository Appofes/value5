/************************************************************************************
����:  Macros.h
�����: ������� �������

�������� ��������������� �������, ����������� ��������, ����� ����������� �� ���� ��-
���� ����������.
************************************************************************************/

#ifndef __MacrosH__
#define __MacrosH__

#include <DxErr.h>

namespace ServiceTools
{
	#define ReleaseCOM(COMInstance)		\
	{									\
		if (COMInstance)				\
		{								\
			COMInstance->Release();		\
			COMInstance = 0;			\
		}								\
	}

	#define HR(x)												\
	{															\
		HRESULT hr = (x);										\
		if (FAILED(hr))                                         \
		{														\
			DXTrace(__FILE__, (DWORD)__LINE__, hr, L#x, true);	\
		}														\
	}

	#define Message(message_text)			\
	{										\
		MessageBox(0, message_text, L"", 0);\
	}
}

#endif//__MacrosH__