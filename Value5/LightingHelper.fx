/************************************************************************************
����:  LightingHelper.fx
�����: ������� �������

�������� �������, ����������� �������� ������ ��������� � ����� ���������, ��������-
������ �������� ���� ������, ����������� ����������� � ����� LightingHelper.h
************************************************************************************/

#ifndef __LightingHelperFX__
#define __LightingHelperFX__

/*
���������, �������������� �������� �����������
*/
struct Material
{
	float4 m_diffuseReflectionColor;						//���� ����������� ��������� �����������
	float4 m_specularReflectionColor;						//���� ����������� ��������� �����������
	float4 m_specularExponent;								//���������� ���������� ������������, ��������� ������ ����� ���������� ���������
	float4 m_reflectionColor;
};


/*
���������, �������������� �������� �������� �����
*/
struct PointLightSource
{
	float4 m_ambientColor;									//���� ������� ���������� �����
	float4 m_intensity;										//������������� ��������� � ������� RGB [0.0, 1.0]
	float3 m_position;										//������� � ������� ������������
	float m_range;											//�������� ���������������
	float3 m_attenuationParameters;							//��������� ���������. �-���������� ������� ��������� ������������� ����������� ������������ ���������, Y � ��������� , Z � ���������������.
	float m_pad;											//���������� ��� ���������� �������� C++-��������� � HLSL-���������
};


/*
��������, �������������� �������� ������������� �����
*/
struct DirectionalLightSource
{
	float4 m_ambientColor;									//���� ������� ���������� �����
	float4 m_intensity;										//������������� ��������� � ������� RGB [0.0, 1.0]
	float3 m_direction;										//����������� ���������������
	float m_pad;											//
};	

/*
���������, �������������� �������� ���������� �����
*/
struct SpotLightSource
{
	float4 m_ambientColor;									//���� ������� ���������� �����
	float4 m_intensity;										//������������� ��������� � ������� RGB [0.0, 1.0]
	float3 m_position;										//������� � ������� ������������
	float m_range;											//�������� ���������������
	float3 m_direction;										//����������� ���������������
	float m_concentrationExponent;							//���������� ������������ �����
	float3 m_attenuationParameters;							//��������� ���������
	float m_pad;											//
};


/*
������� ��������� ����������� ��������� ��� ��������� �� ��������� ����������.
*/
float ComputeAttenuation(float3 attenuation_parameters, float distance)
{
	return ( attenuation_parameters.x + (attenuation_parameters.y * distance) + (attenuation_parameters.z * pow(distance, 2)) );
}

/*
������� ��������� ������, ����������� �� ����� �� �������� �������� �����.
*/
float3 ComputeDirectionToLight(PointLightSource point_light_source, float3 surface_point_position)
{
	return normalize(point_light_source.m_position - surface_point_position);
}

/*
������� ��������� ������, ����������� �� ����� �� �������� ������������� �����.
*/
float3 ComputeDirectionToLight(DirectionalLightSource directional_light_source)
{
	return normalize(-directional_light_source.m_direction);
}

/*
������� ��������� ������, ����������� �� ����� �� �������� ���������� �����.
*/
float3 ComputeDirectionToLight(SpotLightSource spot_light_source, float3 surface_point_position)
{
	return normalize(spot_light_source.m_position - surface_point_position);
}

/*
������� ��������� ������, ����������� �� ����� �� �������.
*/
float3 ComputeDirectionToViewer(float3 surface_point_position, float3 viewer_position)
{
	return normalize(viewer_position - surface_point_position);
}

/*
������� ��������� ������ ��������� ��������� �� ����� �����.
*/
float3 ComputeReflectionVector(float3 direction_to_light, float3 surface_point_normal)
{
	return normalize( 2 * dot(dot(surface_point_normal, direction_to_light), surface_point_normal) - direction_to_light );
}

/*
������� ��������� ������������� ����� ��������� ���������, ���������� ����� �����������.
*/
float4 ComputeIntensityAtPoint(PointLightSource point_light_source, float3 surface_point_position)
{
	if (length(point_light_source.m_position - surface_point_position) > point_light_source.m_range)
	{
		return float4(0.0f, 0.0f, 0.0f, 1.0f);
	}

	float4 numerator = point_light_source.m_intensity;
	float denominator = ComputeAttenuation(point_light_source.m_attenuationParameters, length(point_light_source.m_position - surface_point_position));
	
	return numerator / denominator;
}

/*
������� ��������� ������������� ����� ��������� ������������� �����, ���������� ����� �����������.
*/
float4 ComputeIntensityAtPoint(DirectionalLightSource directional_light_source)
{
	return directional_light_source.m_intensity;
}

/*
������� ��������� ������������� ����� ��������� ���������� �����, ���������� ����� �����������.
*/
float4 ComputeIntensityAtPoint(SpotLightSource spot_light_source, float3 surface_point_position)
{
	if (length(spot_light_source.m_position - surface_point_position) > spot_light_source.m_range)
	{
		return float4(0.0f, 0.0f, 0.0f, 1.0f);
	}

	float3 directionToLight = ComputeDirectionToLight(spot_light_source, surface_point_position);
	float4 numerator = spot_light_source.m_intensity * pow( max(dot(-spot_light_source.m_direction, directionToLight), 0.0f), spot_light_source.m_concentrationExponent );
	float denominator = ComputeAttenuation(spot_light_source.m_attenuationParameters, length(spot_light_source.m_position - surface_point_position));

	return numerator / denominator;
}

/*
������� ��������� ���� ����������� ��������� ����� �� ��������� ��������� �����.
*/
float4 ComputeDiffuseTerm(PointLightSource point_light_source, float3 surface_point_position, float3 surface_point_normal, float4 texelColor, Material material)
{
	float4 ambientPart = material.m_diffuseReflectionColor * texelColor * point_light_source.m_ambientColor;
	float4 diffusePart = material.m_diffuseReflectionColor * texelColor * ComputeIntensityAtPoint(point_light_source, surface_point_position) * 
		max(dot(surface_point_normal, ComputeDirectionToLight(point_light_source, surface_point_position)), 0.0f);

	return ambientPart + diffusePart;
}

/*
������� ��������� ���� ����������� ��������� ����� �� ��������� ������������� �����.
*/
float4 ComputeDiffuseTerm(DirectionalLightSource directional_light_source, float3 surface_point_position, float3 surface_point_normal, float4 texelColor, Material material)
{
	float4 ambientPart = material.m_diffuseReflectionColor * texelColor * directional_light_source.m_ambientColor;
	float4 diffusePart = material.m_diffuseReflectionColor * texelColor * ComputeIntensityAtPoint(directional_light_source) *
		max(dot(surface_point_normal, ComputeDirectionToLight(directional_light_source)), 0.0f);

	return ambientPart + diffusePart;
}

/*
������� ��������� ���� ����������� ��������� ����� �� ��������� ���������� �����.
*/
float4 ComputeDiffuseTerm(SpotLightSource spot_light_source, float3 surface_point_position, float3 surface_point_normal, float4 texelColor, Material material)
{
	float4 ambientPart = material.m_diffuseReflectionColor * texelColor * spot_light_source.m_ambientColor;
	float4 diffusePart = material.m_diffuseReflectionColor * texelColor * ComputeIntensityAtPoint(spot_light_source, surface_point_position) *
		max(dot(surface_point_normal, ComputeDirectionToLight(spot_light_source, surface_point_position)), 0.0f);

	return ambientPart + diffusePart;
}

/*
������� ��������� ���� ����������� ��������� ����� �� ��������� ��������� �����.
*/
float4 ComputeSpecularTerm(PointLightSource point_light_source, float3 surface_point_position, float3 surface_point_normal, float3 direction_to_viewer, float4 texelColor, Material material)
{
	float3 reflectionVector = ComputeReflectionVector(ComputeDirectionToLight(point_light_source, surface_point_position), surface_point_normal);

	int isIlluminated = 0;
	if (0 < dot(surface_point_normal, ComputeDirectionToLight(point_light_source, surface_point_position)))
	{
		isIlluminated = 1;
	}

	return material.m_specularReflectionColor * texelColor * ComputeIntensityAtPoint(point_light_source, surface_point_position) * 
		pow(max(dot(reflectionVector, direction_to_viewer), 0), material.m_specularExponent.x) * (isIlluminated);
}

/*
������� ��������� ���� ����������� ��������� ����� �� ��������� ������������� �����.
*/
float4 ComputeSpecularTerm(DirectionalLightSource directional_light_source, float3 surface_point_position, float3 surface_point_normal, float3 direction_to_viewer, float4 texelColor, Material material)
{
	float3 reflectionVector = ComputeReflectionVector(ComputeDirectionToLight(directional_light_source), surface_point_normal);
	
	int isIlluminated = 0;
	if (0 < dot(surface_point_normal, ComputeDirectionToLight(directional_light_source)))
	{
		isIlluminated = 1;
	}
	
	return material.m_specularReflectionColor * texelColor * ComputeIntensityAtPoint(directional_light_source) *
		pow(max(dot(reflectionVector, direction_to_viewer), 0), material.m_specularExponent.x) * (isIlluminated);
}

/*
������� ��������� ���� ����������� ��������� ����� �� ��������� ���������� �����.
*/
float4 ComputeSpecularTerm(SpotLightSource spot_light_source, float3 surface_point_position, float3 surface_point_normal, float3 direction_to_viewer, float4 texelColor, Material material)
{
	float3 reflectionVector = ComputeReflectionVector(ComputeDirectionToLight(spot_light_source, surface_point_position), surface_point_normal);

	int isIlluminated = 0;
	if (0 < dot(surface_point_normal, ComputeDirectionToLight(spot_light_source, surface_point_position)))
	{
		isIlluminated = 1;
	}

	return material.m_specularReflectionColor * texelColor * ComputeIntensityAtPoint(spot_light_source, surface_point_position) *
		pow(max(dot(reflectionVector, direction_to_viewer), 0), material.m_specularExponent.x) * (isIlluminated);
}

float3 BumpNormalToWorldNormal(float3 normal_map_sample, float3 normal_w, float4 tangent_w)
{
	float3 normalT = 2.0f * normal_map_sample - 1.0f;

	float3 n = normal_w;
	float3 t = normalize((float3)tangent_w - dot(tangent_w, n) * n);

	tangent_w.w = (tangent_w.w < 0) ? -1.0f : 1.0f;
	float3 b = cross(n, t) * tangent_w.w;

	float3x3 tbn = float3x3(t, b, n);

	float3 bumpedNormalW = mul(normalT, tbn);

	return bumpedNormalW;
}

#endif//__LightingHelperFX__