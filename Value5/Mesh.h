#pragma once

#include "SimpleGraphicsObject.h"
#include <string>
#include <assimp.hpp>
#include <aiScene.h>

namespace GraphicsSystem
{
	class Mesh : public GraphicsSystem::SimpleGraphicsObject  {
	private:
		int m_numIndices;
	public:
		Mesh(const aiMesh* mesh, const aiMaterial* material, const std::string& working_directory);

		void draw(DRAW_MODE_FLAGS flags = DRAW_MODE_FLAGS::NONE);
		void update(float delta_time);
		void setWorldMatrix(D3DXMATRIX& world_matrix);
		void setTextureTransformMatrix(D3DXMATRIX& texture_transform_matrix);
		D3DXMATRIX worldMatrix();

		~Mesh();
	};
}