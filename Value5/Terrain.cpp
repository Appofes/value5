#include "Terrain.h"
#include <fstream>
#include <cinttypes>
#include <vector>
#include <cassert>

using namespace GraphicsSystem;
using namespace std;

Terrain::Terrain(const wstring& heightmap_filename,
	const wstring colormap_filenames[NUM_COLOR_MAPS],
	const wstring& blendmap_filename,
	float height_scale,
	int heightmap_width,
	int heightmap_height,
	float cell_spacing)
{
	// TODO
}

void Terrain::loadHeightmap(const wstring& heightmap_filename)
{
	vector<char> heightmapData(m_heightMapWidth * m_heightMapHeight);

	ifstream heightmap(heightmap_filename.c_str(), ios::binary);
	assert(heightmap.good());

	heightmap.read(heightmapData.data(), heightmapData.size());

	m_heightMap.resize(heightmapData.size());

	for (int i = 0; i < heightmapData.size(); i++)
	{
		m_heightMap[i] = (heightmapData[i] / 255.0f) * m_heightScale;
	}
}
