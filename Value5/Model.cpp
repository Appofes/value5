#include "Model.h"
#include <cassert>
#include <aiPostProcess.h>

using namespace std;
using namespace GraphicsSystem;

Model::Model(const string& model_filename)
{
	assert(0 < model_filename.size());

	Assimp::Importer importer;

	const aiScene* scene = importer.ReadFile(model_filename, aiPostProcessSteps::aiProcess_MakeLeftHanded | 
		aiPostProcessSteps::aiProcess_FlipUVs | 
		aiPostProcessSteps::aiProcess_FlipWindingOrder |
		aiPostProcessSteps::aiProcess_Triangulate |
		aiPostProcessSteps::aiProcess_GenNormals);
	assert(scene != nullptr);

	for (int i = 0; i < scene->mNumMeshes; i++)
	{
		aiMesh* mesh = scene->mMeshes[i];
		aiMaterial* material = scene->mMaterials[scene->mMeshes[i]->mMaterialIndex];
		string working_directory = model_filename.substr(0, model_filename.rfind('\\') + 1);

		m_meshes.push_back(new Mesh(mesh, material, working_directory));
	}
}

void Model::draw(DRAW_MODE_FLAGS flags)
{
	for (auto mesh : m_meshes)
	{
		mesh->draw();
	}
}

void Model::update(float delta_time)
{
	for (auto mesh : m_meshes)
	{
		mesh->update(delta_time);
	}
}

void Model::setWorldMatrix(D3DXMATRIX& world_matrix)
{
	for (auto mesh : m_meshes)
	{
		mesh->setWorldMatrix(world_matrix);
	}
}

Model::~Model()
{
	for (Mesh* mesh : m_meshes)
	{
		delete mesh;
	}
}