/************************************************************************************
����:  SimpleGraphicsObject.h
�����: SimpleGraphicsObject
�����: ������� �������

����������� �����, ��������������� ��������� �������� ��������������� �������, �.� 
�������, ��������� �������� ����� ������ ������� ��� ��������������.
************************************************************************************/

#ifndef __SimpleGraphicsObjectH__
#define __SimpleGraphicsObjectH__

#include <D3DX11.h>
#include <D3DX10.h>
#include <d3dx11effect.h>
#include "LightingHelper.h"

namespace GraphicsSystem
{
	enum DRAW_MODE_FLAGS { NONE, USE_ENVIRONMENT_MAPPING };

	class SimpleGraphicsObject
	{
	protected:
		ID3D11Buffer*			m_pVertexBuffer;
		ID3D11Buffer*			m_pIndexBuffer;
		ID3D11InputLayout*		m_pInputLayout;
		ID3DX11Effect*			m_pEffect;
		ID3DX11EffectTechnique* m_pTechnique;
		D3DXMATRIX				m_worldMatrix;
		D3DXMATRIX				m_textureTransformMatrix;
		UINT m_vertexStride;
		UINT m_vertexOffset;
		UINT m_indexOffset;
		GraphicsSystem::Material<D3DXVECTOR4> m_material;
	public:
		virtual void draw(DRAW_MODE_FLAGS flags = DRAW_MODE_FLAGS::NONE) = 0;
		virtual void update(float delta_time) = 0;
		virtual void setWorldMatrix(D3DXMATRIX& world_matrix) = 0;
		virtual void setTextureTransformMatrix(D3DXMATRIX& texture_transform_matrix) = 0;
		virtual D3DXMATRIX worldMatrix() = 0;
		virtual ~SimpleGraphicsObject(){};

		friend __forceinline void ShadowCast(SimpleGraphicsObject* p_object, D3DXPLANE* p_plane);
	};
}

#endif//__SimpleGraphicsObjectH__

