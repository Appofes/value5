cbuffer cbPerObject
{
	float4x4 g_worldViewProj;
};

struct VertexIn
{
	float3 m_positionL  : POSITION;
	float4 m_color		: COLOR;
};

struct VertexOut
{
	float4 m_positionH  : SV_POSITION;
	float4 m_color		: COLOR;
};

VertexOut VS(VertexIn in_vertex)
{
	VertexOut outVertex;
		
	outVertex.m_positionH = mul( float4(in_vertex.m_positionL, 1.0f), g_worldViewProj);	
	outVertex.m_color = in_vertex.m_color;

	return outVertex;
}

float4 PS(VertexOut pin) : SV_Target
{
	float4 black = { 0.0f, 0.0f, 0.0f, 0.0f };
	//return black;
	return pin.m_color;
}

technique11 ColorTech
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_5_0, VS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_5_0, PS()));
	}
}
