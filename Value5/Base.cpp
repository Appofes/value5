/************************************************************************************
����:  Base.cpp
�����: Base
�����: ������� �������

����������� �������, �������������� ������������� �����������, ����������� �������
������(�������� �����), ������������ ��������.
************************************************************************************/

#include <fstream>
#include "Base.h"
#include "Box.h"
#include "Quad.h"
#include "Macros.h"
#include "Camera.h"
#include "Colors.h"
#include "SpecialEffects.h"
#include "Planes.h"
#include "MatrixTransforms.h"
#include <cassert>

using namespace GraphicsSystem;
using namespace ServiceTools;
using namespace MathTools;
using namespace std;

Base::Base()
{
	m_pDevice = NULL;
	m_pContext = NULL;
	m_pSwapChain = NULL;
	m_pDepthStencilBuffer = NULL;
	m_pRenderTargetView = NULL;
	m_pDepthStencilView = NULL;

	m_clientWindowWidth = DEFAULT_CLIENT_WINDOW_WIDTH;
	m_clientWindowHeight = DEFAULT_CLIENT_WINDOW_HEIGHT;
	m_toEnableMSAA = DEFAULT_ENABLE_MSAA_STATE;
	m_mainWindowHandler = 0;

	ZeroMemory(&m_viewPort, sizeof(D3D11_VIEWPORT));

	m_pBox = 0;
	m_pWall = 0;
	m_pFloor = 0;
	m_model = nullptr;
	m_sky = nullptr;
}

Base::Base(Base& initializer)
{
	INTENTIONALLY_EMPTY
}

Base& Base::operator=(Base& initializer)
{
	return *this;
}

Base& Base::getInstance()
{
	static Base baseSingleton;
	return baseSingleton;
}

bool Base::initializeWindow(HINSTANCE h_instance)
{
	WNDCLASS wc;
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = MainWindowProcedure;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = h_instance;
	wc.hIcon = LoadIcon(0, IDI_APPLICATION);
	wc.hCursor = LoadCursor(0, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(NULL_BRUSH);
	wc.lpszMenuName = 0;
	wc.lpszClassName = L"Value5";

	if (!RegisterClass(&wc))
	{
		MessageBox(0, L"RegisterClass Failed.", 0, 0);
		return false;
	}

	RECT R = { 0, 0, m_clientWindowWidth, m_clientWindowHeight };
	AdjustWindowRect(&R, WS_OVERLAPPEDWINDOW, false);
	int width = R.right - R.left;
	int height = R.bottom - R.top;

	m_mainWindowHandler = CreateWindow(L"Value5", L"Value5",
		WS_EX_TOPMOST, 0, 0, width, height, 0, 0, h_instance, 0);
	if (!m_mainWindowHandler)
	{
		MessageBox(0, L"CreateWindow Failed.", 0, 0);
		return false;
	}

	ShowWindow(m_mainWindowHandler, SW_SHOW);
	UpdateWindow(m_mainWindowHandler);

	return true;
}

bool Base::initializeDirect3D()
{
	D3D_FEATURE_LEVEL featureLevel;
	HRESULT hr = D3D11CreateDevice(
		0,
		DEFAULT_DRIVER_TYPE,
		0,
		0,
		0,
		0,
		D3D11_SDK_VERSION,
		&m_pDevice,
		&featureLevel,
		&m_pContext);

	if (FAILED(hr))
	{
		MessageBox(0, L"D3D11CreateDevice() - FAILED", 0, 0);
		return false;
	}
	if (featureLevel != D3D_FEATURE_LEVEL_11_0)
	{
		MessageBox(0, L"Direct3D 11 unsupported", 0, 0);
		return false;
	}

	UINT msaaQuality = 0;
	m_pDevice->CheckMultisampleQualityLevels(DXGI_FORMAT_R8G8B8A8_UNORM, 4, &msaaQuality);
	assert(msaaQuality > 0);

	DXGI_SWAP_CHAIN_DESC sd;
	sd.BufferDesc.Width = m_clientWindowWidth;
	sd.BufferDesc.Height = m_clientWindowHeight;
	sd.BufferDesc.RefreshRate.Numerator = 60;
	sd.BufferDesc.RefreshRate.Denominator = 1;
	sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	sd.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	sd.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

	if (m_toEnableMSAA)
	{
		sd.SampleDesc.Count = 4;
		sd.SampleDesc.Quality = msaaQuality - 1;
	}
	else
	{
		sd.SampleDesc.Count = 1;
		sd.SampleDesc.Quality = 0;
	}

	sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	sd.BufferCount = 1;
	sd.OutputWindow = m_mainWindowHandler;
	sd.Windowed = true;
	sd.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	sd.Flags = 0;

	IDXGIDevice* dxgiDevice = 0;
	m_pDevice->QueryInterface(__uuidof(IDXGIDevice), (void**)&dxgiDevice);

	IDXGIAdapter* dxgiAdapter = 0;
	dxgiDevice->GetParent(__uuidof(IDXGIAdapter), (void**)&dxgiAdapter);

	IDXGIFactory* dxgiFactory = 0;
	dxgiAdapter->GetParent(__uuidof(IDXGIFactory), (void**)&dxgiFactory);

	dxgiFactory->CreateSwapChain(m_pDevice, &sd, &m_pSwapChain);

	dxgiDevice->Release();
	dxgiAdapter->Release();
	dxgiFactory->Release();

	ID3D11Texture2D* pBackBuffer = 0;
	m_pSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&pBackBuffer));
	m_pDevice->CreateRenderTargetView(pBackBuffer, 0, &m_pRenderTargetView);
	pBackBuffer->Release();

	D3D11_TEXTURE2D_DESC depthStencilDesc;

	depthStencilDesc.Width = m_clientWindowWidth;
	depthStencilDesc.Height = m_clientWindowHeight;
	depthStencilDesc.MipLevels = 1;
	depthStencilDesc.ArraySize = 1;
	depthStencilDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;

	if (m_toEnableMSAA)
	{
		depthStencilDesc.SampleDesc.Count = 4;
		depthStencilDesc.SampleDesc.Quality = msaaQuality - 1;
	}
	else
	{
		depthStencilDesc.SampleDesc.Count = 1;
		depthStencilDesc.SampleDesc.Quality = 0;
	}

	depthStencilDesc.Usage = D3D11_USAGE_DEFAULT;
	depthStencilDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthStencilDesc.CPUAccessFlags = 0;
	depthStencilDesc.MiscFlags = 0;

	m_pDevice->CreateTexture2D(&depthStencilDesc, 0, &m_pDepthStencilBuffer);
	m_pDevice->CreateDepthStencilView(m_pDepthStencilBuffer, 0, &m_pDepthStencilView);

	m_pContext->OMSetRenderTargets(1, &m_pRenderTargetView, m_pDepthStencilView);

	m_viewPort.TopLeftX = 0;
	m_viewPort.TopLeftY = 0;
	m_viewPort.Width = static_cast<float>(m_clientWindowWidth);
	m_viewPort.Height = static_cast<float>(m_clientWindowHeight);
	m_viewPort.MinDepth = 0.0f;
	m_viewPort.MaxDepth = 1.0f;

	m_pContext->RSSetViewports(1, &m_viewPort);

	//
	D3D11_TEXTURE2D_DESC textureDesc;
	textureDesc.Width = ENVIRONMENT_MAP_DIMENSION;
	textureDesc.Height = ENVIRONMENT_MAP_DIMENSION;
	textureDesc.MipLevels = 0;
	textureDesc.ArraySize = 6;
	textureDesc.SampleDesc.Count = 1;
	textureDesc.SampleDesc.Quality = 0;
	textureDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	textureDesc.CPUAccessFlags = 0;
	textureDesc.MiscFlags = D3D11_RESOURCE_MISC_GENERATE_MIPS | D3D11_RESOURCE_MISC_TEXTURECUBE;

	ID3D11Texture2D* cubeTexture = nullptr;
	HR(m_pDevice->CreateTexture2D(&textureDesc, nullptr, &cubeTexture));

	D3D11_RENDER_TARGET_VIEW_DESC renderTargetViewDesc;
	renderTargetViewDesc.Format = textureDesc.Format;
	renderTargetViewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2DARRAY;
	renderTargetViewDesc.Texture2DArray.MipSlice = 0;

	renderTargetViewDesc.Texture2DArray.ArraySize = 1;

	for (int i = 0; i < 6; i++)
	{
		renderTargetViewDesc.Texture2DArray.FirstArraySlice = i;
		HR(m_pDevice->CreateRenderTargetView(cubeTexture, &renderTargetViewDesc, &m_environmentMapRenderTargetView[i]));
	}

	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
	srvDesc.Format = textureDesc.Format;
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURECUBE;
	srvDesc.TextureCube.MostDetailedMip = 0;
	srvDesc.TextureCube.MipLevels = -1;

	HR(m_pDevice->CreateShaderResourceView(cubeTexture, &srvDesc, &m_environmentMapSRV));

	ReleaseCOM(cubeTexture);

	D3D11_TEXTURE2D_DESC depthTexDesc;
	depthTexDesc.Width = ENVIRONMENT_MAP_DIMENSION;
	depthTexDesc.Height = ENVIRONMENT_MAP_DIMENSION;
	depthTexDesc.MipLevels = 1;
	depthTexDesc.ArraySize = 1;
	depthTexDesc.SampleDesc.Count = 1;
	depthTexDesc.SampleDesc.Quality = 0;
	depthTexDesc.Format = DXGI_FORMAT_D32_FLOAT;
	depthTexDesc.Usage = D3D11_USAGE_DEFAULT;
	depthTexDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthTexDesc.CPUAccessFlags = 0;
	depthTexDesc.MiscFlags = 0;

	ID3D11Texture2D* depthTex = nullptr;
	HR(m_pDevice->CreateTexture2D(&depthTexDesc, nullptr, &depthTex));

	D3D11_DEPTH_STENCIL_VIEW_DESC dsvDesc;
	dsvDesc.Format = depthTexDesc.Format;
	dsvDesc.Flags = 0;
	dsvDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	dsvDesc.Texture2D.MipSlice = 0;
	
	HR(m_pDevice->CreateDepthStencilView(depthTex, &dsvDesc, &m_environmentMapDepthStencilView));

	ReleaseCOM(depthTex);

	m_environmentMapViewport.TopLeftX = 0.0f;
	m_environmentMapViewport.TopLeftY = 0.0f;
	m_environmentMapViewport.Width = ENVIRONMENT_MAP_DIMENSION;
	m_environmentMapViewport.Height = ENVIRONMENT_MAP_DIMENSION;
	m_environmentMapViewport.MinDepth = 0.0f;
	m_environmentMapViewport.MaxDepth = 1.0f;

	return true;
}

bool Base::initializeDirectInput(HINSTANCE instance_handler)
{
	HR(DirectInput8Create(instance_handler, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&m_directInput, nullptr));

	HR(m_directInput->CreateDevice(GUID_SysKeyboard, &m_keyboard, nullptr));
	HR(m_directInput->CreateDevice(GUID_SysMouse, &m_mouse, nullptr));

	HR(m_keyboard->SetDataFormat(&c_dfDIKeyboard));
	HR(m_keyboard->SetCooperativeLevel(m_mainWindowHandler, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE));

	HR(m_mouse->SetDataFormat(&c_dfDIMouse));
	HR(m_mouse->SetCooperativeLevel(m_mainWindowHandler, DISCL_EXCLUSIVE | DISCL_NOWINKEY | DISCL_FOREGROUND));

	return true;
}

DIMOUSESTATE Base::getMouseState()
{
	m_mouse->Acquire();

	DIMOUSESTATE mouseState;
	HR(m_mouse->GetDeviceState(sizeof(DIMOUSESTATE), &mouseState));

	return mouseState;
}

vector<uint8_t> Base::getKeyboardState()
{
	m_keyboard->Acquire();

	vector<uint8_t> keyboardState(256);
	HR(m_keyboard->GetDeviceState(keyboardState.size() * sizeof(uint8_t), keyboardState.data()));

	return keyboardState;
}

Sky* Base::sky()
{
	return m_sky;
}

ID3D11ShaderResourceView* Base::environmentMapSRV()
{
	return m_environmentMapSRV;
}

bool Base::setup()
{
	// must be first
	m_sky = new Sky("Textures\\desertcube1024.dds");
	
	m_pBox = new Box(L"Textures\\WoodCrate02.dds");

	D3DXMATRIX wallTranslation;
	D3DXMatrixTranslation(&wallTranslation, 0.0f, 1.0f, 25.0f);
	D3DXMATRIX wallScaling;
	D3DXMatrixScaling(&wallScaling, 25.0f, 25.0f, 1.0f);
	D3DXMATRIX wallTranslation1;
	D3DXMatrixTranslation(&wallTranslation1, 0.0f, -4.0f, 0.0f);
	m_pWall = new Quad(L"Textures\\brick01.dds");
	m_pWall->setWorldMatrix(wallTranslation * wallScaling * wallTranslation1);
	m_pWall->setTextureTransformMatrix(wallScaling);	
	
	D3DXMATRIX T;
	D3DXMATRIX rotationBy90DegreesAboveX;
	D3DXMATRIX S;
	D3DXMatrixScaling(&S, 25.0f, 25.0f, 25.0f);
	FLOAT angle = D3DX_PI / 2.0f;
	D3DXMatrixRotationX(&rotationBy90DegreesAboveX, angle);
	D3DXMatrixTranslation(&T, 0.0f, -4.0f, 0.0f);
	m_pFloor = new Quad(L"Textures\\checkboard.dds");
	m_pFloor->setWorldMatrix(S * rotationBy90DegreesAboveX * T);
	m_pFloor->setTextureTransformMatrix(S);
		
	readLightSourcesConfiguration(0);

	//m_model = new Model("Models\\Mig-29_Fulcrum\\Mig-29_Fulcrum.obj");
	m_model = new Model("Models\\t50_pak-fa\\T-50.obj");
	//D3DXMATRIX rotZ;
	//D3DXMatrixRotationX(&rotZ, D3DX_PI / 2.0f);

	//m_model->setWorldMatrix(rotZ);
	
	return true;
}

bool Base::update(float delta_time)
{
	CAMERA.setLens(0.5f * D3DX_PI, 1.0f, 0.1f, 1000.0f);
	
	////D3DXVECTOR3 center = { -10.0f, 0.0f, -3.0f };
	auto center = WorldMatrixToPosition(m_pWall->worldMatrix());
	center.y = 0.0f;
	
	D3DXVECTOR3 targets[6] =
	{
		{ center.x + 1.0f, center.y, center.z },
		{ center.x - 1.0f, center.y, center.z },
		{ center.x, center.y + 1.0f, center.z },
		{ center.x, center.y - 1.0f, center.z },
		{ center.x, center.y, center.z + 1.0f },
		{ center.x, center.y, center.z - 1.0f }
	};

	const D3DXVECTOR3 ups[6] = 
	{
		{ 0.0f, 1.0f, 0.0f },
		{ 0.0f, 1.0f, 0.0f },
		{ 0.0f, 0.0f, -1.0f },
		{ 0.0f, 0.0f, 1.0f },
		{ 0.0f, 1.0f, 0.0f },
		{ 0.0f, 1.0f, 0.0f }
	};

	auto cachedPos = CAMERA.position();
	auto cachedLook = CAMERA.look();
	auto cachedRight = CAMERA.right();
	
	for (int i = 0; i < 6; i++)
	{
		CAMERA.lookAt(D3DXVECTOR3(center), targets[i], ups[i]);
		CAMERA.constructViewMatrix();
		
		m_pContext->RSSetViewports(1, &m_environmentMapViewport);

		m_pContext->ClearRenderTargetView(m_environmentMapRenderTargetView[i], (float*)&BLUE_COLOR);
		m_pContext->ClearDepthStencilView(m_environmentMapDepthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

		ID3D11RenderTargetView* renderTargets[1] = { m_environmentMapRenderTargetView[i] };

		m_pContext->OMSetRenderTargets(1, renderTargets, m_environmentMapDepthStencilView);

		if (m_pFloor)
		{
			m_pFloor->update(delta_time);
			m_pFloor->draw();
		}
		if (m_model != nullptr)
		{
			m_model->update(delta_time);
			m_model->draw();
		}
		if (m_sky != nullptr)
		{
			m_sky->update(delta_time);
			m_sky->draw();
		}
	}

	m_pContext->RSSetViewports(1, &m_viewPort);
	
	ID3D11RenderTargetView* renderTargets[1] = { m_pRenderTargetView };
	m_pContext->OMSetRenderTargets(1, renderTargets, m_pDepthStencilView);

	m_pContext->GenerateMips(m_environmentMapSRV);

	CAMERA.setPosition(cachedPos);
	CAMERA.setLook(cachedLook);
	CAMERA.setRight(cachedRight);
	CAMERA.constructViewMatrix();

	CAMERA.setLens(D3DX_PI * 0.5f, (float)DEFAULT_CLIENT_WINDOW_WIDTH / (float)DEFAULT_CLIENT_WINDOW_HEIGHT, 1.0f, 1000.0f);

	CAMERA.update(delta_time);

	if (m_pBox)
	{
		//m_pBox->update(delta_time);
	}
	if (m_pWall)
	{
		m_pWall->update(delta_time);
	}	
	if (m_pFloor)
	{
		m_pFloor->update(delta_time);
	}
	if (m_model != nullptr)
	{
		m_model->update(delta_time);
	}
	if (m_sky != nullptr)
	{
		m_sky->update(delta_time);
	}

	return true;
}

bool Base::display(float delta_time)
{
	m_pContext->ClearRenderTargetView(m_pRenderTargetView, reinterpret_cast<const float*>(&BLACK_COLOR));
	m_pContext->ClearDepthStencilView(m_pDepthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
	
	if (m_pWall)
	{
		m_pWall->draw(DRAW_MODE_FLAGS::USE_ENVIRONMENT_MAPPING);
	}
	if (m_pFloor)
	{
		m_pFloor->draw();
	}
	if (m_pBox)
	{
		//m_pBox->draw();
	}
	if (m_model != nullptr)
	{
		m_model->draw();
	}
	if (m_sky != nullptr)
	{
		m_sky->draw();
	}

	//ShadowCast(m_pBox, &D3DXPLANE(0.0f, 1.0f, 0.0f, 3.99f));
	
	m_pSwapChain->Present(0, 0);

	return true;
}

ID3D11Device* Base::getDevicePointer()
{
	return m_pDevice;
}

ID3D11DeviceContext* Base::getContextPointer()
{
	return m_pContext;
}

void Base::readLightSourcesConfiguration(char* p_config_file_name)
{
	if (!p_config_file_name)
	{
		p_config_file_name = DEFAULT_CONFIG_FILE_NAME;
	}

	ifstream inFile;
	inFile.open(p_config_file_name, std::ios::binary);
	
	if (!inFile)
	{
		MessageBox(0, L"readLightSourcesConfiguration() - FAILED", 0, 0);
	}

	inFile.read((char*)(&m_pointLightSource), sizeof(PointLightSource<D3DXVECTOR4, D3DXVECTOR3>));
	inFile.read(reinterpret_cast<char*>(&m_directionalLightSource), sizeof(DirectionalLightSource<D3DXVECTOR4, D3DXVECTOR3>));
	inFile.read(reinterpret_cast<char*>(&m_spotLightSource), sizeof(SpotLightSource<D3DXVECTOR4, D3DXVECTOR3>));
	
	inFile.close();
}

PointLightSource<D3DXVECTOR4, D3DXVECTOR3> Base::getPointLightSource()
{
	return m_pointLightSource;
}

DirectionalLightSource<D3DXVECTOR4, D3DXVECTOR3> Base::getDirectionalLightSource()
{
	return m_directionalLightSource;
}

SpotLightSource<D3DXVECTOR4, D3DXVECTOR3> Base::getSpotLightSource()
{
	return m_spotLightSource;
}

int Base::gameLoop()
{
	MSG msg;														//���������
	::ZeroMemory(&msg, sizeof(MSG));

	float lastTime = (float)timeGetTime();

	while (msg.message != WM_QUIT)									//��� ��������� � ������
	{
		if (::PeekMessage(&msg, 0, 0, 0, PM_REMOVE))				//���� ��������� � �������?
		{
			::TranslateMessage(&msg);								//������� ���
			::DispatchMessage(&msg);
		}
		else														//�����- ���������� ����������
		{
			float currTime = (float)timeGetTime();					//������� �����
			float deltaTime = (currTime - lastTime) * 0.001f;		//�����, ��������� � ������� ��������� ����������� �����

			update(deltaTime);										//��������� �����
			display(deltaTime);										//������ �����

			lastTime = currTime;									//���������� ����� ��������� ���������� �����
		}
	}
	return msg.wParam;
}

LRESULT CALLBACK MainWindowProcedure(HWND hwnd, UINT msg, WPARAM w_param, LPARAM l_param)
{
	return BASE.messageProcedure(hwnd, msg, w_param, l_param);
}

LRESULT Base::messageProcedure(HWND hwnd, UINT msg, WPARAM w_param, LPARAM l_param)
{
	switch (msg)
	{
	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	case WM_KEYDOWN:
		if (w_param == VK_ESCAPE)
			DestroyWindow(hwnd);
		break;
	}

	return DefWindowProc(hwnd, msg, w_param, l_param);
}

Base::~Base()
{
	ReleaseCOM(m_pDevice);
	ReleaseCOM(m_pContext);
	ReleaseCOM(m_pSwapChain);
	ReleaseCOM(m_pDepthStencilBuffer);
	ReleaseCOM(m_pRenderTargetView);
	ReleaseCOM(m_pDepthStencilView);
	ReleaseCOM(m_environmentMapDepthStencilView);

	for (auto rtv : m_environmentMapRenderTargetView)
	{
		ReleaseCOM(rtv);
	}
	
	ReleaseCOM(m_environmentMapSRV);

	delete m_pBox;
	delete m_pFloor;
	delete m_pWall;
	delete m_model;
	delete m_sky;
}
