/************************************************************************************
����:  BlendingStates.fx
�����: ������� �������

�������� ��������������� ������ ����������.
************************************************************************************/

#ifndef __BlendingStatesFX__
#define __BlendingStatesFX__

BlendState AlphaBlending
{
	BlendEnable[0]	         = TRUE;
	SrcBlend[0]				 = SRC_ALPHA;
	DestBlend[0]			 = INV_SRC_ALPHA;
	BlendOp[0]				 = ADD;
	SrcBlendAlpha[0]		 = ONE;
	DestBlendAlpha[0]		 = ZERO;
	BlendOpAlpha[0]			 = ADD;
	RenderTargetWriteMask[0] = 0x0F;
};

#endif//__BlendingStatesFX__