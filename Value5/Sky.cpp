#include "Sky.h"
#include "Base.h"
#include "VertexFormats.h"
#include "Macros.h"
#include "EffectHelper.h"
#include "Camera.h"
#include <vector>
#include <cassert>

using namespace std;
using namespace GraphicsSystem;
using namespace ServiceTools;

Sky::Sky(const std::string& cubemap_filename)
{
	assert(0 < cubemap_filename.size());

	m_vertexStride = sizeof(TextureVertex);
	m_vertexOffset = 0;
	m_indexOffset = 0;
	D3DXMatrixIdentity(&m_worldMatrix);
	D3DXMatrixIdentity(&m_textureTransformMatrix);

	vector<TextureVertex> vertices;

	TextureVertex topVertex;
	topVertex.m_position = { 0.0f, RADIUS, 0.0f };
	
	vertices.push_back(topVertex);

	float phiStep = D3DX_PI / STACK_COUNT;
	float thetaStep = 2.0f * D3DX_PI / SLICE_COUNT;

	for (int i = 1; i <= STACK_COUNT - 1; i++)
	{
		float phi = i * phiStep;

		for (int j = 0; j <= SLICE_COUNT; j++)
		{
			float theta = j * thetaStep;

			TextureVertex vertex;
			vertex.m_position = { RADIUS * sinf(phi) * cosf(theta), RADIUS * cosf(phi), RADIUS * sinf(phi) * sinf(theta) };

			vertices.push_back(vertex);
		}
	}

	TextureVertex bottomVertex;
	bottomVertex.m_position = { 0.0f, -RADIUS, 0.0f };

	vertices.push_back(bottomVertex);

	vector<uint32_t> indices;

	for (int i = 1; i <= SLICE_COUNT; i++)
	{
		indices.push_back(0);
		indices.push_back(i + 1);
		indices.push_back(i);
	}

	uint32_t baseIndex = 1;
	uint32_t ringVertexCount = SLICE_COUNT + 1;

	for (int i = 0; i < STACK_COUNT - 2; i++)
	{
		for (int j = 0; j < SLICE_COUNT; j++)
		{
			indices.push_back(baseIndex + i * ringVertexCount + j);
			indices.push_back(baseIndex + i * ringVertexCount + j + 1);
			indices.push_back(baseIndex + (i + 1) * ringVertexCount + j);

			indices.push_back(baseIndex + (i + 1) * ringVertexCount + j);
			indices.push_back(baseIndex + i * ringVertexCount + j + 1);
			indices.push_back(baseIndex + (i + 1) * ringVertexCount + j + 1);
		}
	}

	uint32_t southPoleIndex = vertices.size() - 1;

	baseIndex = southPoleIndex - ringVertexCount;

	for (int i = 0; i < SLICE_COUNT; i++)
	{
		indices.push_back(southPoleIndex);
		indices.push_back(baseIndex + i);
		indices.push_back(baseIndex + i + 1);
	}

	m_numIndices = indices.size();

	D3D11_BUFFER_DESC vertexBufferDesc;
	vertexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	vertexBufferDesc.ByteWidth = sizeof(TextureVertex) * vertices.size();
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA vertexBufferInitData;
	vertexBufferInitData.pSysMem = vertices.data();
	HR(BASE.getDevicePointer()->CreateBuffer(&vertexBufferDesc, &vertexBufferInitData, &m_pVertexBuffer));

	D3D11_BUFFER_DESC indexBufferDesc;
	indexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	indexBufferDesc.ByteWidth = sizeof(uint32_t) * indices.size();
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA indexBufferInitData;
	indexBufferInitData.pSysMem = indices.data();
	HR(BASE.getDevicePointer()->CreateBuffer(&indexBufferDesc, &indexBufferInitData, &m_pIndexBuffer));

	InitializeEffect("SkySphere.fxo", &m_pEffect);

	//ID3D11ShaderResourceView* cubemapSRV = nullptr;
	wstring cubemapFilename(cubemap_filename.begin(), cubemap_filename.end());
	HR(D3DX11CreateShaderResourceViewFromFileW(BASE.getDevicePointer(), cubemapFilename.data(), nullptr, nullptr, &m_cubemapSRV, nullptr));

	auto cubeMap = m_pEffect->GetVariableByName("g_cubeMap")->AsShaderResource();
	cubeMap->SetResource(m_cubemapSRV);

	ReleaseCOM(cubeMap);

	m_pTechnique = m_pEffect->GetTechniqueByName("SkySphere");
	m_pTechnique->GetPassByIndex(0)->Apply(0, BASE.getContextPointer());

	D3DX11_PASS_DESC passDesc;
	m_pTechnique->GetPassByIndex(0)->GetDesc(&passDesc);

	HR(BASE.getDevicePointer()->CreateInputLayout(pTextureVertexDescription, 3, passDesc.pIAInputSignature, passDesc.IAInputSignatureSize, &m_pInputLayout));
}

void Sky::draw(DRAW_MODE_FLAGS flags)
{
	auto context = BASE.getContextPointer();

	context->IASetInputLayout(m_pInputLayout);
	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	context->IASetVertexBuffers(0, 1, &m_pVertexBuffer, &m_vertexStride, &m_vertexOffset);
	context->IASetIndexBuffer(m_pIndexBuffer, DXGI_FORMAT_R32_UINT, m_indexOffset);

	D3DX11_TECHNIQUE_DESC techDesc;
	m_pTechnique->GetDesc(&techDesc);

	for (int i = 0; i < techDesc.Passes; i++)
	{
		m_pTechnique->GetPassByIndex(i)->Apply(0, context);
		context->DrawIndexed(m_numIndices, 0, 0);
	}
}

void Sky::update(float delta_time)
{
	auto effectWorldViewProj = m_pEffect->GetVariableByName("g_worldViewProj")->AsMatrix();
	
	D3DXMatrixIdentity(&m_worldMatrix);
	auto cameraPosition = CAMERA.position();
	D3DXMatrixTranslation(&m_worldMatrix, cameraPosition.x, cameraPosition.y, cameraPosition.z);

	auto worldViewProj = m_worldMatrix * CAMERA.viewMatrix() * CAMERA.projMatrix();
	effectWorldViewProj->SetMatrix((float*)&worldViewProj);

	ReleaseCOM(effectWorldViewProj);
}

void Sky::setWorldMatrix(D3DXMATRIX& world_matrix)
{
	m_worldMatrix = world_matrix;
}

void Sky::setTextureTransformMatrix(D3DXMATRIX& texture_transform_matrix)
{
	m_textureTransformMatrix = texture_transform_matrix;
}

D3DXMATRIX Sky::worldMatrix()
{
	return m_worldMatrix;
}

ID3D11ShaderResourceView* Sky::cubemapSRV()
{
	return m_cubemapSRV;
}

Sky::~Sky()
{
	ReleaseCOM(m_cubemapSRV);//
	ReleaseCOM(m_pVertexBuffer);
	ReleaseCOM(m_pIndexBuffer);
	ReleaseCOM(m_pInputLayout);
	ReleaseCOM(m_pTechnique);
	ReleaseCOM(m_pEffect);
}

