/************************************************************************************
����:  LightingHelper.h
�����: ������� �������

����� ��������, �������������� �������� �������, �������� �������� �����, ��������
������������� �����, �������� ���������� �����.
************************************************************************************/

#ifndef __LightingHelperH__
#define __LightingHelperH__

namespace GraphicsSystem
{
	const float DEFAULT_SPECULAR_EXPONENT = 1.0f;
	const D3DXVECTOR4 DEFAULT_SPECULAR_EXPONENT_AS_VECTOR = {DEFAULT_SPECULAR_EXPONENT, DEFAULT_SPECULAR_EXPONENT, DEFAULT_SPECULAR_EXPONENT, DEFAULT_SPECULAR_EXPONENT};
	const float DEFAULT_CONCENTRATION_EXPONENT = 10.0f;
	const D3DXVECTOR4 NO_REFLECTION = { 0.0f, 0.0f, 0.0f, 1.0f };
 
	template <typename Vector4D>
	struct Material
	{
		Vector4D m_diffuseReflectionColor;
		Vector4D m_specularReflectionColor;
		Vector4D m_specularExponent;						//{specularExponent, specularExponent, specularExponent, specularExponent}
		Vector4D m_reflectionColor;
	};

	template <typename Vector4D, typename Vector3D>
	struct PointLightSource
	{
		Vector4D m_ambientColor;
		Vector4D m_intensity;
		Vector3D m_position;
		float m_range;
		Vector3D m_attenuationParameters;
		float m_pad;
	};

	template <typename Vector4D, typename Vector3D>
	struct DirectionalLightSource
	{
		Vector4D m_ambientColor;
		Vector4D m_intensity;
		Vector3D m_direction;
		float m_pad;				
	};

	template <typename Vector4D, typename Vector3D>
	struct SpotLightSource
	{
		Vector4D m_ambientColor;
		Vector4D m_intensity;
		Vector3D m_position;
		float m_range;
		Vector3D m_direction;
		float m_concentrationExponent;
		Vector3D m_attenuationParameters;
		float m_pad;
	};
}

#endif//__LightingHelperH__