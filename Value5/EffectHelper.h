/************************************************************************************
����:  EffectHelper.h
�����: ������� �������

�������� �������, ���������� �������� � ���������.
************************************************************************************/

#ifndef __EffectHelperH__
#define __EffectHelperH__

#include <d3dx11effect.h>
#include <D3DX10.h>
#include <iostream>
#include <fstream>
#include <vector>
#include "Base.h"
#include "Macros.h"
#include "LightingHelper.h"
#include "Camera.h"
#include "MatrixTransforms.h"
#include "Materials.h"

using namespace std;
using namespace GraphicsSystem;

namespace ServiceTools
{
	__forceinline void InitializeEffect(char* p_compiled_effect_file_name, ID3DX11Effect** pp_effect)
	{
		std::ifstream fin(p_compiled_effect_file_name, std::ios::binary);
		fin.seekg(0, std::ios_base::end);
		int size = (int)fin.tellg();
		fin.seekg(0, std::ios_base::beg);
		std::vector<char> compiledShader(size);
		fin.read(&compiledShader[0], size);
		fin.close();
		HR(D3DX11CreateEffectFromMemory(&compiledShader[0], size, 0, BASE.getDevicePointer(), pp_effect));
	}

	__forceinline void InitializeEffect(LPCWSTR p_effect_file_name, ID3DX11Effect** pp_effect)
	{
		DWORD shaderFlags = 0;
		ID3D10Blob* compiledShader = 0;
		ID3D10Blob* compilationMsgs = 0;
		HRESULT hr = D3DX11CompileFromFile(p_effect_file_name, 0, 0, 0, "fx_5_0", shaderFlags,
			0, 0, &compiledShader, &compilationMsgs, 0);

		if (compilationMsgs != 0)
		{
			MessageBoxA(0, (char*)compilationMsgs->GetBufferPointer(), 0, 0);
			ReleaseCOM(compilationMsgs);
		}

		if (FAILED(hr))
		{
			DXTrace(__FILE__, (DWORD)__LINE__, hr, L"D3DX11CompileFromFile", true);
		}

		HR(D3DX11CreateEffectFromMemory(compiledShader->GetBufferPointer(), compiledShader->GetBufferSize(),
			0, BASE.getDevicePointer(), pp_effect));

		ReleaseCOM(compiledShader);
	}	

	__forceinline void UpdateEffectBase(D3DXMATRIX* p_world_matrix, D3DXMATRIX* p_texture_transform_matrix, Material<D3DXVECTOR4>* p_material, LPCWSTR p_texture_file_name, ID3DX11Effect* p_effect)
	{
		if (p_effect)
		{
			if (p_world_matrix)
			{
				ID3DX11EffectMatrixVariable* pEffectWorldMatrix = p_effect->GetVariableByName("g_worldMatrix")->AsMatrix();
				pEffectWorldMatrix->SetMatrix(reinterpret_cast<float*>(p_world_matrix));

				D3DXMATRIX normalCorrectionMatrix = MathTools::NormalCorrectionTransformation(*p_world_matrix);
				ID3DX11EffectMatrixVariable* pEffectNormalCorrectionMatrix = p_effect->GetVariableByName("g_normalCorrectionMatrix")->AsMatrix();
				pEffectNormalCorrectionMatrix->SetMatrix(reinterpret_cast<float*>(&normalCorrectionMatrix));

				ReleaseCOM(pEffectWorldMatrix);
				ReleaseCOM(pEffectNormalCorrectionMatrix);
			}
			if (p_texture_transform_matrix)
			{
				ID3DX11EffectMatrixVariable* pTextureTransformMatrix = p_effect->GetVariableByName("g_textureTransformMatrix")->AsMatrix();
				pTextureTransformMatrix->SetMatrix(reinterpret_cast<float*>(p_texture_transform_matrix));

				ReleaseCOM(pTextureTransformMatrix);
			}
			if (p_material)
			{
				ID3DX11EffectVariable* pEffectMaterial = p_effect->GetVariableByName("g_objectMaterial");
				pEffectMaterial->SetRawValue(p_material, 0, sizeof(*p_material));
				
				ReleaseCOM(pEffectMaterial);
			}
			if (p_texture_file_name)
			{
				//������� ������������������ �� 400%!!! �������� ��� ������� ������ ��������������� ���� ��� �� ����� �������������!
				ID3D11ShaderResourceView* pTextureShaderResourceView = 0;
				HR(D3DX11CreateShaderResourceViewFromFileW(BASE.getDevicePointer(), p_texture_file_name, 0, 0, &pTextureShaderResourceView, 0));
				ID3DX11EffectShaderResourceVariable* pTexture = p_effect->GetVariableByName("g_objectTexture")->AsShaderResource();
				pTexture->SetResource(pTextureShaderResourceView);
				
				ReleaseCOM(pTexture);

				auto skyCubemap = p_effect->GetVariableByName("g_skyCubemap")->AsShaderResource();//
				//skyCubemap->SetResource(BASE.sky()->cubemapSRV());//
				skyCubemap->SetResource(BASE.environmentMapSRV());////

				ReleaseCOM(skyCubemap);//

				auto toUseEnvironmentMapping = p_effect->GetVariableByName("g_toUseEnvironmentMapping")->AsScalar();
				toUseEnvironmentMapping->SetBool(false);

				ReleaseCOM(toUseEnvironmentMapping);
			}			

			D3DXMATRIX viewMatrix = CAMERA.viewMatrix();
			ID3DX11EffectMatrixVariable* pEffectViewMatrix = p_effect->GetVariableByName("g_viewMatrix")->AsMatrix();
			pEffectViewMatrix->SetMatrix(reinterpret_cast<float*>(&viewMatrix));
			ReleaseCOM(pEffectViewMatrix);

			D3DXMATRIX projectionMatrix = CAMERA.projMatrix();
			ID3DX11EffectMatrixVariable* pEffectProjectionMatrix = p_effect->GetVariableByName("g_projectionMatrix")->AsMatrix();
			pEffectProjectionMatrix->SetMatrix(reinterpret_cast<float*>(&projectionMatrix));
			ReleaseCOM(pEffectProjectionMatrix);
			
			D3DXVECTOR3 viewerPosition = CAMERA.position();
			ID3DX11EffectVariable* pEffectViewerPosition = p_effect->GetVariableByName("g_viewerPositionW");
			pEffectViewerPosition->SetRawValue(&viewerPosition, 0, sizeof(viewerPosition));
			ReleaseCOM(pEffectViewerPosition);

			PointLightSource<D3DXVECTOR4, D3DXVECTOR3> pointLightSource = BASE.getPointLightSource();
			ID3DX11EffectVariable* pEffectPointLightSource = p_effect->GetVariableByName("g_pointLightSource");
			pEffectPointLightSource->SetRawValue(&pointLightSource, 0, sizeof(pointLightSource));
			ReleaseCOM(pEffectPointLightSource);

			DirectionalLightSource<D3DXVECTOR4, D3DXVECTOR3> directionalLightSource = BASE.getDirectionalLightSource();
			ID3DX11EffectVariable* pEffectDirectionalLightSource = p_effect->GetVariableByName("g_directionalLightSource");
			pEffectDirectionalLightSource->SetRawValue(&directionalLightSource, 0, sizeof(directionalLightSource));
			ReleaseCOM(pEffectDirectionalLightSource);

			SpotLightSource<D3DXVECTOR4, D3DXVECTOR3> spotLightSource = BASE.getSpotLightSource();
			ID3DX11EffectVariable* pEffectSpotLightSource = p_effect->GetVariableByName("g_spotLightSource");
			pEffectSpotLightSource->SetRawValue(&spotLightSource, 0, sizeof(spotLightSource));
			ReleaseCOM(pEffectSpotLightSource);

			//ID3DX11EffectTechnique* pTechnique = p_effect->GetTechniqueByName("Lighting");
			//pTechnique->GetPassByIndex(0)->Apply(0, BASE.getContextPointer());			
		}

	}

}

#endif//__EffectHelperH__