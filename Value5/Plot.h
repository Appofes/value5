#ifndef __PlotH__
#define __PlotH__

#include "SimpleGraphicsObject.h"
using namespace GraphicsSystem;

namespace GraphicsSystem
{
	class Plot : public SimpleGraphicsObject
	{
	private:
		int m_numberOfPoints;
	public:
		Plot(D3DXVECTOR2* p_points, int number_of_points, float line_thickness);
		void draw(DRAW_MODE_FLAGS flags = DRAW_MODE_FLAGS::NONE);
		void update(float delta_time);
		void setWorldMatrix(D3DXMATRIX& world_matrix);
		void setTextureTransformMatrix(D3DXMATRIX& texture_transform_matrix);
		D3DXMATRIX worldMatrix();
		~Plot();
	};
}


#endif//__PlotH__