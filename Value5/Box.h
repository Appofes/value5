/************************************************************************************
����:  Box.h
�����: Box
�����: ������� �������

�����, ����������� ��������� SimpleGraphicsObject, �������������� ��������� ����.
************************************************************************************/


#ifndef __BoxH__
#define __BoxH__

#include "SimpleGraphicsObject.h"
using namespace GraphicsSystem;

namespace GraphicsSystem
{
	class Box : public SimpleGraphicsObject
	{
	public:
		Box(LPCWSTR p_texture_file_name);
		void draw(DRAW_MODE_FLAGS flags = DRAW_MODE_FLAGS::NONE);
		void update(float delta_time);
		void setWorldMatrix(D3DXMATRIX& world_matrix);
		void setTextureTransformMatrix(D3DXMATRIX& texture_transform_matrix);
		D3DXMATRIX worldMatrix();
		~Box();
	};
}

#endif//__BoxH__