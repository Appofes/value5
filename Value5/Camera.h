/************************************************************************************

�����: Camera (Camera.h)

�����: ������� �������

�����, ����������� ������ �� ������� ����.

************************************************************************************/

#ifndef __CameraH__
#define __CameraH__

#include <D3DX11.h>
#include <D3DX10.h>
#include <xnamath.h>

#define CAMERA Camera::getInstance()

namespace GraphicsSystem
{
	const D3DXVECTOR3 DEFAULT_CAMERA_POSITION = { 0.0f, 0.0f, 0.0f };
	const D3DXVECTOR3 DEFAULT_CAMERA_LOOK = { 0.0f, 0.0f, 1.0f };
	const D3DXVECTOR3 DEFAULT_CAMERA_RIGHT = { 1.0f, 0.0f, 0.0f };
	const D3DXVECTOR3 DEFAULT_CAMERA_UP = { 0.0f, 1.0f, 0.0f };
	const float DEFAULT_CAMERA_PITCH = 10.0f;	//
	const float DEFAULT_FOV_Y = D3DX_PI * 0.5f;
	const float DEFAULT_NEAR_Z = 1.0f;
	const float DEFAULT_FAR_Z = 1000.0f;

	class Camera
	{
	private:		
		D3DXVECTOR3 m_position;
		D3DXVECTOR3 m_look;
		D3DXVECTOR3 m_right;
		D3DXVECTOR3 m_up;
		D3DXMATRIX m_viewMatrix;
		D3DXMATRIX m_projMatrix;
		float m_nearZ;
		float m_farZ;
		float m_aspectRatio;
		float m_fovY;
		float m_nearY;
		float m_farY;
	private:
		Camera();
		Camera(Camera& initializer);
		Camera& operator=(Camera& initializer);
	public:
		__forceinline void constructViewMatrix();
		
		static Camera& getInstance();
		~Camera();

		D3DXVECTOR3 position();
		void setPosition(D3DXVECTOR3& position);
		
		D3DXVECTOR3 look();
		void setLook(D3DXVECTOR3& look);
		
		D3DXMATRIX viewMatrix();
		D3DXMATRIX projMatrix();

		D3DXVECTOR3 up();
		void setUp(const D3DXVECTOR3& up);

		D3DXVECTOR3 right();
		void setRight(const D3DXVECTOR3& right);
				
		void setLens(float fov_y, float aspect_ratio, float near_z, float far_z);
		
		float fovX();
		float nearX();
		float nearY();
		float farX();
		float farY();

		void walk(float distance);
		void strafe(float distance);
		void pitch(float angle);
		void rotateY(float angle);

		void lookAt(const D3DXVECTOR3& position, const D3DXVECTOR3& target, const D3DXVECTOR3& up);

		void update(float dt);
	};
}

#endif//__CameraH__