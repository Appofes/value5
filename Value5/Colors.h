/************************************************************************************
����:  Colors.h
�����: ������� �������

�������� ��������������� ���������, �������������� �����.
************************************************************************************/

#ifndef __ColorsH__
#define __ColorsH__

#include <D3DX10.h>

namespace ServiceTools
{
	const D3DXVECTOR4 WHITE_COLOR = {1.0f, 1.0f, 1.0f, 1.0f};
	const D3DXVECTOR4 BLACK_COLOR = { 0.0f, 0.0f, 0.0f, 1.0f };
	const D3DXVECTOR4 RED_COLOR = { 1.0f, 0.0f, 0.0f, 1.0f };
	const D3DXVECTOR4 GREEN_COLOR = { 0.0f, 1.0f, 0.0f, 1.0f };
	const D3DXVECTOR4 BLUE_COLOR = { 0.0f, 0.0f, 1.0f, 1.0f };
	const D3DXVECTOR4 YELLOW_COLOR = { 1.0f, 1.0f, 0.0f, 1.0f };
	const D3DXVECTOR4 CYAN_COLOR = { 0.0f, 1.0f, 1.0f, 1.0f };
	const D3DXVECTOR4 MAGENTA_COLOR = { 1.0f, 0.0f, 1.0f, 1.0f };

	const D3DXVECTOR4 SILVER_COLOR = { 0.75f, 0.75f, 0.75f, 1.0f };
	const D3DXVECTOR4 LIGHT_STEEL_BLUE = { 0.69f, 0.77f, 0.87f, 1.0f };
}

#endif//__ColorsH__