/************************************************************************************
����:  Camera.cpp
�����: Camera
�����: ������� �������

����������� �������, �������������� ������������� ������, ���������� � ���������.
************************************************************************************/

#include "Camera.h"
#include "Base.h"
#include "MatrixTransforms.h"
using namespace GraphicsSystem;
using namespace MathTools;

Camera::Camera()
{
	m_position = DEFAULT_CAMERA_POSITION;
	m_look = DEFAULT_CAMERA_LOOK;
	m_right = DEFAULT_CAMERA_RIGHT;
	m_up = DEFAULT_CAMERA_UP;
	
	this->setLens(DEFAULT_FOV_Y, (float)DEFAULT_CLIENT_WINDOW_WIDTH / (float)DEFAULT_CLIENT_WINDOW_HEIGHT, DEFAULT_NEAR_Z, DEFAULT_FAR_Z);
}

Camera::Camera(Camera& initializer)
{
	INTENTIONALLY_EMPTY
}

Camera& Camera::operator=(Camera& initializer)
{
	return *this;
}

Camera& Camera::getInstance()
{
	static Camera camera;
	return camera;
}

D3DXVECTOR3 Camera::position()
{
	return m_position;
}

void Camera::setPosition(D3DXVECTOR3& position)
{
	m_position = position;
}

D3DXVECTOR3 Camera::look()
{
	return m_look;
}

void Camera::setLook(D3DXVECTOR3& look)
{
	m_look = look;
}

void Camera::setUp(const D3DXVECTOR3& up)
{
	m_up = up;
}

D3DXVECTOR3 Camera::up()
{
	return m_up;
}

void Camera::setRight(const D3DXVECTOR3& right)
{
	m_right = right;
}

D3DXVECTOR3 Camera::right()
{
	return m_right;
}

D3DXMATRIX Camera::viewMatrix()
{
	return m_viewMatrix;
}

void Camera::setLens(float fov_y, float aspect_ratio, float near_z, float far_z)
{
	m_fovY = fov_y;
	m_aspectRatio = aspect_ratio;
	m_nearZ = near_z;
	m_farZ = far_z;

	m_nearY = 2.0f * m_nearZ * tanf(0.5f * m_fovY);
	m_farY = 2.0f * m_farZ * tanf(0.5f * m_fovY);

	D3DXMatrixPerspectiveFovLH(&m_projMatrix, m_fovY, m_aspectRatio, m_nearZ, m_farZ);
}

D3DXMATRIX Camera::projMatrix()
{
	return m_projMatrix;
}

float Camera::fovX()
{
	float halfWidth = 0.5f * this->nearX();

	return 2.0f * atan(halfWidth / m_nearZ);
}

float Camera::nearX()
{
	return m_aspectRatio * m_nearY;
}

float Camera::nearY()
{
	return m_nearY;
}

float Camera::farX()
{
	return m_aspectRatio * m_farY;
}

float Camera::farY()
{
	return m_farY;
}

void Camera::walk(float distance)
{
	m_position += distance * m_look;
}

void Camera::strafe(float distance)
{
	m_position += distance * m_right;
}

void Camera::pitch(float angle)
{
	D3DXMATRIX r;
	D3DXMatrixRotationAxis(&r, &m_right, angle);

	D3DXVec3TransformNormal(&m_up, &m_up, &r);
	D3DXVec3TransformNormal(&m_look, &m_look, &r);
}

void Camera::rotateY(float angle)
{
	D3DXMATRIX r;
	D3DXMatrixRotationY(&r, angle);

	D3DXVec3TransformNormal(&m_right, &m_right, &r);
	D3DXVec3TransformNormal(&m_up, &m_up, &r);
	D3DXVec3TransformNormal(&m_look, &m_look, &r);
}

void Camera::lookAt(const D3DXVECTOR3& position, const D3DXVECTOR3& target, const D3DXVECTOR3& up)
{
	D3DXVec3Subtract(&m_look, &target, &position);
	D3DXVec3Normalize(&m_look, &m_look);

	D3DXVec3Cross(&m_right, &up, &m_look);
	D3DXVec3Normalize(&m_right, &m_right);

	D3DXVec3Cross(&m_up, &m_look, &m_right);

	m_position = position;
}

__forceinline void Camera::constructViewMatrix()
{
	D3DXVec3Normalize(&m_look, &m_look);

	D3DXVec3Cross(&m_up, &m_look, &m_right);

	D3DXVec3Normalize(&m_up, &m_up);

	D3DXVec3Cross(&m_right, &m_up, &m_look);

	float x = -D3DXVec3Dot(&m_position, &m_right);
	float y = -D3DXVec3Dot(&m_position, &m_up);
	float z = -D3DXVec3Dot(&m_position, &m_look);

	m_viewMatrix(0, 0) = m_right.x;
	m_viewMatrix(1, 0) = m_right.y;
	m_viewMatrix(2, 0) = m_right.z;
	m_viewMatrix(3, 0) = x;
	m_viewMatrix(0, 1) = m_up.x;
	m_viewMatrix(1, 1) = m_up.y;
	m_viewMatrix(2, 1) = m_up.z;
	m_viewMatrix(3, 1) = y;
	m_viewMatrix(0, 2) = m_look.x;
	m_viewMatrix(1, 2) = m_look.y;
	m_viewMatrix(2, 2) = m_look.z;
	m_viewMatrix(3, 2) = z;
	m_viewMatrix(0, 3) = 0.0f;
	m_viewMatrix(1, 3) = 0.0f;
	m_viewMatrix(2, 3) = 0.0f;
	m_viewMatrix(3, 3) = 1.0f;
}

void Camera::update(float dt)
{
	auto keyboardState = BASE.getKeyboardState();

	if (keyboardState[DIK_W] & 0x80)
	{
		this->walk(DEFAULT_CAMERA_PITCH * dt);
	}
	if (keyboardState[DIK_S] & 0x80)
	{
		this->walk(-DEFAULT_CAMERA_PITCH * dt);
	}
	if (keyboardState[DIK_A] & 0x80)
	{
		this->strafe(-DEFAULT_CAMERA_PITCH * dt);
	}
	if (keyboardState[DIK_D] & 0x80)
	{
		this->strafe(DEFAULT_CAMERA_PITCH * dt);
	}

	auto mouseState = BASE.getMouseState();
	
	float dx = D3DXToRadian(0.25f * (float)(mouseState.lX));
	float dy = D3DXToRadian(0.25f * (float)(mouseState.lY));

	this->pitch(dy);
	this->rotateY(dx);

	this->constructViewMatrix();
}

Camera::~Camera()
{

}