/************************************************************************************
����:  LightingModel.fx
�����: ������� �������

������� ������ ����������. �������� ������ ������ � ������ ��� ����.
************************************************************************************/

#ifndef __LightingModelFX__
#define __LightingModelFX__

#include "Base.fx"
#include "DepthStencilStates.fx"
#include "BlendingStates.fx"
#include "SamplerStates.fx"
#include "LightingHelper.fx"

const float4 DEFAULT_BLEND_FACTOR = (0.0f, 0.0f, 0.0f, 0.0f);
const uint	 DEFAULT_SAMPLE_MASK = 0xffffffff;
const uint   DEFAULT_STENCIL_REFERENCE_VALUE = 0;

struct InVertex
{
	float3 m_positionL : POSITION;
	float3 m_normalL   : NORMAL;
	float4 m_tangentL  : TANGENT;
	float2 m_texelL	   : TEXCOORD;
};

struct OutVertex {
	float4 m_positionH : SV_POSITION;
	float3 m_positionW : POSITION;
	float3 m_normalW : NORMAL;
	float4 m_tangentW : TANGENT;
	float2 m_texelL : TEXCOORD;
};

//struct OutPrimitive
//{
//	float4 m_positionH : SV_POSITION;
//	float3 m_positionW : POSITION;
//	float3 m_normalW   : NORMAL;
//	float4 m_tangentW  : TANGENT;
//	float2 m_texelL	   : TEXCOORD;
//};

//InVertex VS(InVertex vertex_in)
//{
//	return vertex_in;
//}

OutVertex VS(InVertex in_vertex)
{
	OutVertex outVertex;

	float4x4 worldViewProj = mul(mul(g_worldMatrix, g_viewMatrix), g_projectionMatrix);

	outVertex.m_positionH = mul(float4(in_vertex.m_positionL, 1.0f), worldViewProj);
	outVertex.m_positionW = mul(float4(in_vertex.m_positionL, 1.0f), g_worldMatrix);
	outVertex.m_normalW = normalize(mul(float4(in_vertex.m_normalL, 1.0f), g_normalCorrectionMatrix));
	outVertex.m_tangentW = float4(mul((float3)in_vertex.m_tangentL, (float3x3)g_worldMatrix), in_vertex.m_tangentL.w);
	outVertex.m_texelL = mul(float4(in_vertex.m_texelL, 0.0f, 1.0f), g_textureTransformMatrix).xy;

	return outVertex;
}

//[maxvertexcount(4)]
//void GS(triangle InVertex in_primitive[3], inout TriangleStream<OutPrimitive> triangle_stream)
//{
//	OutPrimitive outPrimitive;
//
//	float4x4 homogeneousClipSpaceTransform = mul(mul(g_worldMatrix, g_viewMatrix), g_projectionMatrix);
//
//	for (int i = 0; i < 3; i++)
//	{
//		outPrimitive.m_positionH = mul(float4(in_primitive[i].m_positionL, 1.0f), homogeneousClipSpaceTransform);
//		outPrimitive.m_positionW = mul(float4(in_primitive[i].m_positionL, 1.0f), g_worldMatrix);
//		outPrimitive.m_normalW = normalize(mul(float4(in_primitive[i].m_normalL, 0.0f), g_normalCorrectionMatrix));
//		outPrimitive.m_texelL = mul(float4(in_primitive[i].m_texelL, 0.0f, 1.0f), g_textureTransformMatrix).xy;
//
//		outPrimitive.m_tangentW = float4(mul((float3)in_primitive[i].m_tangentL, (float3x3)g_worldMatrix), in_primitive[i].m_tangentL.w);
//
//		triangle_stream.Append(outPrimitive);
//	}
//
//	triangle_stream.RestartStrip();
//}

float4 PS(/*OutPrimitive*/OutVertex surface_point) : SV_TARGET
{
	surface_point.m_normalW = normalize(surface_point.m_normalW);
	float3 directionToViewer = ComputeDirectionToViewer(surface_point.m_positionW, g_viewerPositionW);

	float3 normalMapSample = g_normalMap.Sample(LinearWrap, surface_point.m_texelL).rgb;
	surface_point.m_normalW = BumpNormalToWorldNormal(normalMapSample, surface_point.m_normalW, surface_point.m_tangentW);

	float4 diffuseTerm = ComputeDiffuseTerm(g_pointLightSource, surface_point.m_positionW, surface_point.m_normalW, g_objectTexture.Sample(LinearWrap, surface_point.m_texelL), g_objectMaterial) +
	ComputeDiffuseTerm(g_directionalLightSource, surface_point.m_positionW, surface_point.m_normalW, g_objectTexture.Sample(LinearWrap, surface_point.m_texelL), g_objectMaterial) +
	ComputeDiffuseTerm(g_spotLightSource, surface_point.m_positionW, surface_point.m_normalW, g_objectTexture.Sample(LinearWrap, surface_point.m_texelL), g_objectMaterial);

	float4 specularTerm = ComputeSpecularTerm(g_pointLightSource, surface_point.m_positionW, surface_point.m_normalW, directionToViewer, g_objectTexture.Sample(LinearWrap, surface_point.m_texelL), g_objectMaterial) +
		ComputeSpecularTerm(g_directionalLightSource, surface_point.m_positionW, surface_point.m_normalW, directionToViewer, g_objectTexture.Sample(LinearWrap, surface_point.m_texelL), g_objectMaterial) +
		ComputeSpecularTerm(g_spotLightSource, surface_point.m_positionW, surface_point.m_normalW, directionToViewer, g_objectTexture.Sample(LinearWrap, surface_point.m_texelL), g_objectMaterial);

	if (g_toUseEnvironmentMapping)
	{
		float3 incident = -directionToViewer;
		float3 reflectionVector = reflect(incident, surface_point.m_normalW);
		float4 reflectionColor = g_skyCubemap.Sample(LinearWrap, reflectionVector);

		// you must take care about oversaturation of pixel color!!!
		return diffuseTerm + specularTerm + g_objectMaterial.m_reflectionColor * reflectionColor;
	}
	else
	{
		return diffuseTerm + specularTerm;
	}
}

float4 PS1(/*OutPrimitive*/OutVertex surface_point) : SV_TARGET
{
	return float4(0.0f, 0.0f, 0.0f, 0.5f);
}

technique11 Lighting
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_5_0, VS()));
		SetHullShader(NULL);
		SetDomainShader(NULL);
		SetGeometryShader(NULL);
		//SetGeometryShader(CompileShader(gs_5_0, GS()));
		SetPixelShader(CompileShader(ps_5_0, PS()));
		SetDepthStencilState(NULL, 0.0f);
		SetBlendState(NULL, DEFAULT_BLEND_FACTOR, DEFAULT_SAMPLE_MASK);
		SetRasterizerState(NULL);//
	}
};

technique11 Shadowing
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_5_0, VS()));
		SetHullShader(NULL);
		SetDomainShader(NULL);
		SetGeometryShader(NULL);
		//SetGeometryShader(CompileShader(gs_5_0, GS()));
		SetPixelShader(CompileShader(ps_5_0, PS1()));
		SetDepthStencilState(NoDoubleBlending, 0.0f);
		SetBlendState(AlphaBlending, DEFAULT_BLEND_FACTOR, DEFAULT_SAMPLE_MASK);
		SetRasterizerState(NULL);//
	}
};

#endif//__LightinModelFX__
