/************************************************************************************
����:  Box.cpp
�����: Box
�����: ������� �������

����������� �������, ���������� �� ������������� � ��������� �������, � ����� ��� ��-
����������.
************************************************************************************/

#include "Box.h"
#include "Base.h"
#include "Macros.h"
#include "Materials.h"
#include "VertexFormats.h"
#include "EffectHelper.h"
using namespace GraphicsSystem;
using namespace ServiceTools;

Box::Box(LPCWSTR p_texture_file_name)
{
	m_pVertexBuffer = 0;
	m_pIndexBuffer = 0;
	m_pInputLayout = 0;
	m_pEffect = 0;
	m_pTechnique = 0;
	m_vertexStride = 0;
	m_vertexOffset = 0;
	m_indexOffset = 0;
	m_material = WHITE_MATERIAL_SHINY;

	m_vertexStride = sizeof(TextureVertex);
	D3DXMatrixIdentity(&m_worldMatrix);
	D3DXMatrixIdentity(&m_textureTransformMatrix);

	float width = 1.0f;
	float height = 1.0f;
	float depth = 1.0f;

	TextureVertex pVertices[24];

	INITIALIZING_VERTEX_BUFFER
	{
		pVertices[0] = { D3DXVECTOR3(-width, -height, -depth), D3DXVECTOR3(0.0f, 0.0f, -1.0f), D3DXVECTOR2(0.0f, 1.0f) };
		pVertices[1] = { D3DXVECTOR3(-width, +height, -depth), D3DXVECTOR3(0.0f, 0.0f, -1.0f), D3DXVECTOR2(0.0f, 0.0f) };
		pVertices[2] = { D3DXVECTOR3(+width, +height, -depth), D3DXVECTOR3(0.0f, 0.0f, -1.0f), D3DXVECTOR2(1.0f, 0.0f) };
		pVertices[3] = { D3DXVECTOR3(+width, -height, -depth), D3DXVECTOR3(0.0f, 0.0f, -1.0f), D3DXVECTOR2(1.0f, 1.0f) };

		pVertices[4] = { D3DXVECTOR3(-width, -height, +depth), D3DXVECTOR3(0.0f, 0.0f, 1.0f), D3DXVECTOR2(1.0f, 1.0f) };
		pVertices[5] = { D3DXVECTOR3(+width, -height, +depth), D3DXVECTOR3(0.0f, 0.0f, 1.0f), D3DXVECTOR2(0.0f, 1.0f) };
		pVertices[6] = { D3DXVECTOR3(+width, +height, +depth), D3DXVECTOR3(0.0f, 0.0f, 1.0f), D3DXVECTOR2(0.0f, 0.0f) };
		pVertices[7] = { D3DXVECTOR3(-width, +height, +depth), D3DXVECTOR3(0.0f, 0.0f, 1.0f), D3DXVECTOR2(1.0f, 0.0f) };

		pVertices[8] = { D3DXVECTOR3(-width, +height, -depth), D3DXVECTOR3(0.0f, 1.0f, 0.0f), D3DXVECTOR2(0.0f, 1.0f) };
		pVertices[9] = { D3DXVECTOR3(-width, +height, +depth), D3DXVECTOR3(0.0f, 1.0f, 0.0f), D3DXVECTOR2(0.0f, 0.0f) };
		pVertices[10] = { D3DXVECTOR3(+width, +height, +depth), D3DXVECTOR3(0.0f, 1.0f, 0.0f), D3DXVECTOR2(1.0f, 0.0f) };
		pVertices[11] = { D3DXVECTOR3(+width, +height, -depth), D3DXVECTOR3(0.0f, 1.0f, 0.0f), D3DXVECTOR2(1.0f, 1.0f) };

		pVertices[12] = { D3DXVECTOR3(-width, -height, -depth), D3DXVECTOR3(0.0f, -1.0f, 0.0f), D3DXVECTOR2(1.0f, 1.0f) };
		pVertices[13] = { D3DXVECTOR3(+width, -height, -depth), D3DXVECTOR3(0.0f, -1.0f, 0.0f), D3DXVECTOR2(0.0f, 1.0f) };
		pVertices[14] = { D3DXVECTOR3(+width, -height, +depth), D3DXVECTOR3(0.0f, -1.0f, 0.0f), D3DXVECTOR2(0.0f, 0.0f) };
		pVertices[15] = { D3DXVECTOR3(-width, -height, +depth), D3DXVECTOR3(0.0f, -1.0f, 0.0f), D3DXVECTOR2(1.0f, 0.0f) };

		pVertices[16] = { D3DXVECTOR3(-width, -height, +depth), D3DXVECTOR3(-1.0f, 0.0f, 0.0f), D3DXVECTOR2(0.0f, 1.0f) };
		pVertices[17] = { D3DXVECTOR3(-width, +height, +depth), D3DXVECTOR3(-1.0f, 0.0f, 0.0f), D3DXVECTOR2(0.0f, 0.0f) };
		pVertices[18] = { D3DXVECTOR3(-width, +height, -depth), D3DXVECTOR3(-1.0f, 0.0f, 0.0f), D3DXVECTOR2(1.0f, 0.0f) };
		pVertices[19] = { D3DXVECTOR3(-width, -height, -depth), D3DXVECTOR3(-1.0f, 0.0f, 0.0f), D3DXVECTOR2(1.0f, 1.0f) };

		pVertices[20] = { D3DXVECTOR3(+width, -height, -depth), D3DXVECTOR3(1.0f, 0.0f, 0.0f), D3DXVECTOR2(0.0f, 1.0f) };
		pVertices[21] = { D3DXVECTOR3(+width, +height, -depth), D3DXVECTOR3(1.0f, 0.0f, 0.0f), D3DXVECTOR2(0.0f, 0.0f) };
		pVertices[22] = { D3DXVECTOR3(+width, +height, +depth), D3DXVECTOR3(1.0f, 0.0f, 0.0f), D3DXVECTOR2(1.0f, 0.0f) };
		pVertices[23] = { D3DXVECTOR3(+width, -height, +depth), D3DXVECTOR3(1.0f, 0.0f, 0.0f), D3DXVECTOR2(1.0f, 1.0f) };
	}

	UINT pIndices[36];

	INITIALIZING_INDEX_BUFFER
	{
		pIndices[0] = 0; pIndices[1] = 1; pIndices[2] = 2;
		pIndices[3] = 0; pIndices[4] = 2; pIndices[5] = 3;

		pIndices[6] = 4; pIndices[7] = 5; pIndices[8] = 6;
		pIndices[9] = 4; pIndices[10] = 6; pIndices[11] = 7;

		pIndices[12] = 8; pIndices[13] = 9; pIndices[14] = 10;
		pIndices[15] = 8; pIndices[16] = 10; pIndices[17] = 11;

		pIndices[18] = 12; pIndices[19] = 13; pIndices[20] = 14;
		pIndices[21] = 12; pIndices[22] = 14; pIndices[23] = 15;

		pIndices[24] = 16; pIndices[25] = 17; pIndices[26] = 18;
		pIndices[27] = 16; pIndices[28] = 18; pIndices[29] = 19;

		pIndices[30] = 20; pIndices[31] = 21; pIndices[32] = 22;
		pIndices[33] = 20; pIndices[34] = 22; pIndices[35] = 23;
	}


	D3D11_BUFFER_DESC vertexBufferDescription;
	vertexBufferDescription.Usage = D3D11_USAGE_IMMUTABLE;
	vertexBufferDescription.ByteWidth = sizeof(TextureVertex)* 24;
	vertexBufferDescription.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDescription.CPUAccessFlags = 0;
	vertexBufferDescription.MiscFlags = 0;
	vertexBufferDescription.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA vertexBufferInitData;
	vertexBufferInitData.pSysMem = pVertices;
	HR(BASE.getDevicePointer()->CreateBuffer(&vertexBufferDescription, &vertexBufferInitData, &m_pVertexBuffer));


	D3D11_BUFFER_DESC indexBufferDescription;
	indexBufferDescription.Usage = D3D11_USAGE_IMMUTABLE;
	indexBufferDescription.ByteWidth = sizeof(UINT)* 36;
	indexBufferDescription.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDescription.CPUAccessFlags = 0;
	indexBufferDescription.MiscFlags = 0;
	indexBufferDescription.StructureByteStride = 0;
	D3D11_SUBRESOURCE_DATA indexBufferInitData;
	indexBufferInitData.pSysMem = pIndices;
	HR(BASE.getDevicePointer()->CreateBuffer(&indexBufferDescription, &indexBufferInitData, &m_pIndexBuffer));

	InitializeEffect("LightingModel.fxo", &m_pEffect);
	UpdateEffectBase(&m_worldMatrix, &m_textureTransformMatrix, &m_material, p_texture_file_name, m_pEffect);

	m_pTechnique = m_pEffect->GetTechniqueByName("Lighting");
	m_pTechnique->GetPassByIndex(0)->Apply(0, BASE.getContextPointer());

	D3DX11_PASS_DESC passDesc;
	m_pTechnique->GetPassByIndex(0)->GetDesc(&passDesc);
	HR(BASE.getDevicePointer()->CreateInputLayout(pTextureVertexDescription, 3, passDesc.pIAInputSignature,
		passDesc.IAInputSignatureSize, &m_pInputLayout));
}

void Box::setWorldMatrix(D3DXMATRIX& world_matrix)
{
	m_worldMatrix = world_matrix;
}

void Box::setTextureTransformMatrix(D3DXMATRIX& texture_transform_matrix)
{
	m_textureTransformMatrix = texture_transform_matrix;
}

D3DXMATRIX Box::worldMatrix()
{
	return m_worldMatrix;
}

void Box::draw(DRAW_MODE_FLAGS flags)
{
	ID3D11DeviceContext* pContext = BASE.getContextPointer();

	pContext->IASetInputLayout(m_pInputLayout);
	pContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	pContext->IASetVertexBuffers(0, 1, &m_pVertexBuffer, &m_vertexStride, &m_vertexOffset);
	pContext->IASetIndexBuffer(m_pIndexBuffer, DXGI_FORMAT_R32_UINT, m_indexOffset);

	D3DX11_TECHNIQUE_DESC techDesc;
	m_pTechnique->GetDesc(&techDesc);
	for (UINT p = 0; p < techDesc.Passes; ++p)
	{
		m_pTechnique->GetPassByIndex(p)->Apply(0, pContext);
		pContext->DrawIndexed(36, 0, 0);
	}
}

void Box::update(float delta_time)
{
	D3DXMATRIX rotationX, rotationY;

	D3DXMatrixRotationX(&rotationX, XM_PI / 4.0f);

	static float y = 0.0f;

	D3DXMatrixRotationY(&rotationY, y);
	y += delta_time;

	if (y >= 6.28f)
	{
		y = 0.0f;
	}

	m_worldMatrix = rotationX * rotationY;
	UpdateEffectBase(&m_worldMatrix, &m_textureTransformMatrix, 0, 0, m_pEffect);
}

Box::~Box()
{	
	ReleaseCOM(m_pVertexBuffer);
	ReleaseCOM(m_pIndexBuffer);
	ReleaseCOM(m_pInputLayout);
	ReleaseCOM(m_pTechnique);
	ReleaseCOM(m_pEffect);			
}
