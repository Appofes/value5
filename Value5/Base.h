/************************************************************************************
����:  Base.h
�����: Base
�����: ������� �������

�����, ���������� �������� ����������, ������������ ������� ������.
************************************************************************************/

#ifndef __BaseH__
#define __BaseH__

#include <D3DX11.h>
#include <D3DX10.h>
#include <dinput.h>
#include "SimpleGraphicsObject.h"
#include "LightingHelper.h"
#include "Model.h"
#include "Sky.h"

#define BASE							Base::getInstance()						
#define INTENTIONALLY_EMPTY														
#define INITIALIZING_INDEX_BUFFER
#define INITIALIZING_VERTEX_BUFFER
#define INITIALIZING_MATRIX
#define DEFAULT_CLIENT_WINDOW_WIDTH		800										
#define DEFAULT_CLIENT_WINDOW_HEIGHT	600										
#define DEFAULT_ENABLE_MSAA_STATE		false
#define DEFAULT_DRIVER_TYPE				D3D_DRIVER_TYPE_HARDWARE
#define DEFAULT_CONFIG_FILE_NAME		"LightsConfiguration.val"
#define ENVIRONMENT_MAP_DIMENSION 256

class Base
{
private:
	ID3D11Device*			m_pDevice;
	ID3D11DeviceContext*	m_pContext;
	IDXGISwapChain*			m_pSwapChain;
	ID3D11Texture2D*		m_pDepthStencilBuffer;
	ID3D11RenderTargetView* m_pRenderTargetView;
	ID3D11DepthStencilView* m_pDepthStencilView;
	IDirectInputDevice8*	m_keyboard;
	IDirectInputDevice8*	m_mouse;
	LPDIRECTINPUT8			m_directInput;
	D3D11_VIEWPORT			m_viewPort;
	HWND					m_mainWindowHandler;
	int						m_clientWindowWidth;
	int						m_clientWindowHeight;
	bool					m_toEnableMSAA;	

	GraphicsSystem::SimpleGraphicsObject* m_pBox;
	GraphicsSystem::SimpleGraphicsObject* m_pWall;
	GraphicsSystem::SimpleGraphicsObject* m_pFloor;
	GraphicsSystem::PointLightSource<D3DXVECTOR4, D3DXVECTOR3> m_pointLightSource;
	GraphicsSystem::DirectionalLightSource<D3DXVECTOR4, D3DXVECTOR3> m_directionalLightSource;
	GraphicsSystem::SpotLightSource<D3DXVECTOR4, D3DXVECTOR3> m_spotLightSource;
	GraphicsSystem::Model* m_model;
	GraphicsSystem::Sky* m_sky;
	ID3D11ShaderResourceView* m_environmentMapSRV;
	ID3D11RenderTargetView* m_environmentMapRenderTargetView[6];
	ID3D11DepthStencilView* m_environmentMapDepthStencilView;
	D3D11_VIEWPORT m_environmentMapViewport;
	//vector<Drawable*> m_scene;
private:
	Base();
	Base(Base& initializer);
	Base& operator=(Base& initializer);
public:
	static Base& getInstance();

	bool initializeWindow(HINSTANCE h_instance);
	bool initializeDirect3D();
	bool initializeDirectInput(HINSTANCE instance_handler);
	bool setup();
	bool display(float delta_time);
	bool update(float delta_time);

	ID3D11Device* getDevicePointer();
	ID3D11DeviceContext* getContextPointer();

	DIMOUSESTATE getMouseState();
	std::vector<uint8_t> getKeyboardState();

	GraphicsSystem::Sky* sky();
	ID3D11ShaderResourceView* environmentMapSRV();
	
	GraphicsSystem::PointLightSource<D3DXVECTOR4, D3DXVECTOR3> getPointLightSource();
	GraphicsSystem::DirectionalLightSource<D3DXVECTOR4, D3DXVECTOR3> getDirectionalLightSource();
	GraphicsSystem::SpotLightSource<D3DXVECTOR4, D3DXVECTOR3> getSpotLightSource();
	
	void readLightSourcesConfiguration(char* p_config_file_name);

	int gameLoop();

	LRESULT messageProcedure(HWND hwnd, UINT msg, WPARAM w_param, LPARAM l_param);

	~Base();
};

LRESULT CALLBACK MainWindowProcedure(HWND hwnd, UINT msg, WPARAM w_param, LPARAM l_param);

#endif//__BaseH__