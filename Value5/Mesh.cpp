#include "Mesh.h"
#include "Base.h"
#include "VertexFormats.h"
#include "EffectHelper.h"
#include "Macros.h"
#include "MatrixTransforms.h"
#include <aiScene.h>
#include <cassert>
#include <cinttypes>

using namespace std;
using namespace GraphicsSystem;
using namespace ServiceTools;

Mesh::Mesh(const aiMesh* mesh, const aiMaterial* material, const string& working_directory)
{
	assert(mesh != nullptr && material != nullptr);

	m_vertexStride = sizeof(BumpVertex);
	m_vertexOffset = 0;
	m_indexOffset = 0;
	D3DXMatrixIdentity(&m_worldMatrix);
	D3DXMatrixIdentity(&m_textureTransformMatrix);

	vector<BumpVertex> vertices(mesh->mNumVertices);

	for (int i = 0; i < vertices.size(); i++)
	{
		vertices[i].m_position.x = mesh->mVertices[i].x;
		vertices[i].m_position.y = mesh->mVertices[i].y;
		vertices[i].m_position.z = mesh->mVertices[i].z;

		if (mesh->HasNormals())
		{
			vertices[i].m_normal.x = mesh->mNormals[i].x;
			vertices[i].m_normal.y = mesh->mNormals[i].y;
			vertices[i].m_normal.z = mesh->mNormals[i].z;
		}

		if (mesh->HasTextureCoords(0))
		{
			vertices[i].m_texCoord.x = mesh->mTextureCoords[0][i].x;
			vertices[i].m_texCoord.y = mesh->mTextureCoords[0][i].y;
		}
	}

	vector<uint32_t> indices(mesh->mNumFaces * 3);
	m_numIndices = mesh->mNumFaces * 3;

	for (int i = 0; i < mesh->mNumFaces; i++)
	{
		indices[i * 3 + 0] = mesh->mFaces[i].mIndices[0];
		indices[i * 3 + 1] = mesh->mFaces[i].mIndices[1];
		indices[i * 3 + 2] = mesh->mFaces[i].mIndices[2];
	}

	MathTools::CalculateTangents(&vertices, indices);
	
	D3D11_BUFFER_DESC vertexBufferDesc;
	vertexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	vertexBufferDesc.ByteWidth = sizeof(BumpVertex) * vertices.size();
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA vertexBufferInitData;
	vertexBufferInitData.pSysMem = vertices.data();
	HR(BASE.getDevicePointer()->CreateBuffer(&vertexBufferDesc, &vertexBufferInitData, &m_pVertexBuffer));

	D3D11_BUFFER_DESC indexBufferDesc;
	indexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	indexBufferDesc.ByteWidth = sizeof(uint32_t) * indices.size();
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA indexBufferInitData;
	indexBufferInitData.pSysMem = indices.data();
	HR(BASE.getDevicePointer()->CreateBuffer(&indexBufferDesc, &indexBufferInitData, &m_pIndexBuffer));

	aiColor3D diffuseColor;
	material->Get(AI_MATKEY_COLOR_DIFFUSE, diffuseColor);
	m_material.m_diffuseReflectionColor = { diffuseColor.r, diffuseColor.g, diffuseColor.b, 1.0f };
	
	aiColor3D specularColor;
	material->Get(AI_MATKEY_COLOR_SPECULAR, specularColor);
	m_material.m_specularReflectionColor = { specularColor.r, specularColor.g, specularColor.b, 1.0f };

	float shininess;
	material->Get(AI_MATKEY_SHININESS, shininess);
	m_material.m_specularExponent = { shininess, shininess, shininess, shininess };

	aiString textureFilename;
	material->GetTexture(aiTextureType::aiTextureType_DIFFUSE, 0, &textureFilename);

	string fullTextureFilename = working_directory + textureFilename.data;

	wstring wTextureFilename(fullTextureFilename.begin(), fullTextureFilename.end());

	InitializeEffect("LightingModelBump.fxo", &m_pEffect);

	if (0 < textureFilename.length)
	{
		UpdateEffectBase(&m_worldMatrix, &m_textureTransformMatrix, &m_material, wTextureFilename.data(), m_pEffect);
	}
	else
	{
		UpdateEffectBase(&m_worldMatrix, &m_textureTransformMatrix, &m_material, nullptr, m_pEffect);
	}

	if (0 < material->GetTextureCount(aiTextureType::aiTextureType_HEIGHT))
	{
		aiString heightMapFilename;
		material->GetTexture(aiTextureType::aiTextureType_HEIGHT, 0, &heightMapFilename);

		string fullHeightMapFilename = working_directory + heightMapFilename.data;
		wstring wFullHeightMapFilename(fullHeightMapFilename.begin(), fullHeightMapFilename.end());

		auto normalMap = m_pEffect->GetVariableByName("g_normalMap")->AsShaderResource();
		
		ID3D11ShaderResourceView* normalMapSRV = nullptr;
		HR(D3DX11CreateShaderResourceViewFromFileW(BASE.getDevicePointer(), wFullHeightMapFilename.data(), nullptr, nullptr, &normalMapSRV, nullptr));

		normalMap->SetResource(normalMapSRV);

		ReleaseCOM(normalMap);
	}

	m_pTechnique = m_pEffect->GetTechniqueByName("Lighting");
	m_pTechnique->GetPassByIndex(0)->Apply(0, BASE.getContextPointer());

	D3DX11_PASS_DESC passDesc;
	m_pTechnique->GetPassByIndex(0)->GetDesc(&passDesc);

	HR(BASE.getDevicePointer()->CreateInputLayout(g_bumpVertexDesc, 4, passDesc.pIAInputSignature, passDesc.IAInputSignatureSize, &m_pInputLayout));
}

void Mesh::draw(DRAW_MODE_FLAGS flags)
{
	ID3D11DeviceContext* context = BASE.getContextPointer();

	context->IASetInputLayout(m_pInputLayout);
	//context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_3_CONTROL_POINT_PATCHLIST);
	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	context->IASetVertexBuffers(0, 1, &m_pVertexBuffer, &m_vertexStride, &m_vertexOffset);
	context->IASetIndexBuffer(m_pIndexBuffer, DXGI_FORMAT_R32_UINT, m_indexOffset);

	D3DX11_TECHNIQUE_DESC techDesc;
	m_pTechnique->GetDesc(&techDesc);

	for (int i = 0; i < techDesc.Passes; i++)
	{
		m_pTechnique->GetPassByIndex(i)->Apply(0, context);
		context->DrawIndexed(m_numIndices, 0, 0);
	}
}

void Mesh::update(float delta_time)
{
	UpdateEffectBase(&m_worldMatrix, &m_textureTransformMatrix, 0, 0, m_pEffect);
}

void Mesh::setWorldMatrix(D3DXMATRIX& world_matrix)
{
	m_worldMatrix = world_matrix;
}

void Mesh::setTextureTransformMatrix(D3DXMATRIX& texture_transform_matrix)
{
	m_textureTransformMatrix = texture_transform_matrix;
}

D3DXMATRIX Mesh::worldMatrix()
{
	return m_worldMatrix;
}

Mesh::~Mesh()
{
	ReleaseCOM(m_pVertexBuffer);
	ReleaseCOM(m_pIndexBuffer);
	ReleaseCOM(m_pInputLayout);
	ReleaseCOM(m_pTechnique);
	ReleaseCOM(m_pEffect);
}