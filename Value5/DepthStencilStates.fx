/************************************************************************************
����:  DepthStencilStates.fx
�����: ������� �������

�������� ��������������� ������ ������ ������ ���������.
************************************************************************************/

#ifndef __DepthStencilStatesFX__
#define __DepthStencilStatesFX__

DepthStencilState NoDoubleBlending
{
	DepthEnable = true;
	DepthWriteMask = ZERO;
	DepthFunc = LESS;
	
	StencilEnable = true;
	StencilReadMask = 0xff;
	StencilWriteMask = 0xff;

	FrontFaceStencilFail = KEEP;
	FrontFaceStencilDepthFail = KEEP;
	FrontFaceStencilPass = INCR;
	FrontFaceStencilFunc = EQUAL;	
	
	BackFaceStencilFail = KEEP;
	BackFaceStencilDepthFail = KEEP;
	BackFaceStencilPass = INCR;
	BackFaceStencilFunc = EQUAL;
};

#endif//__DepthStencilStatesFX__