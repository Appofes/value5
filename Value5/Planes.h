/************************************************************************************
����:  Planes.h
�����: ������� �������

�������� �������, ���������� � �����������.
************************************************************************************/

#ifndef __PlanesH__
#define __PlanesH__

#include <D3DX10.h>

namespace MathTools
{
	__forceinline D3DXPLANE PlaneByThreeDots(float x1, float y1, float z1, float x2, float y2, float z2, float x3, float y3, float z3)
	{
		float A1 = (y2 - y1)*(z3 - z1) - (y3 - y1)*(z2 - z1);
		float B1 = -((x2 - x1)*(z3 - z1) - (x3 - x1)*(z2 - z1));
		float C1 = (x2 - x1)*(y3 - y1) - (x3 - x1)*(y2 - y1);
		float D = -x1*A1 - y1*B1 - z1*C1;

		return D3DXPLANE(A1, B1, C1, D);
	}

	__forceinline D3DXPLANE PlaneByThreeDots(D3DXVECTOR3& point1, D3DXVECTOR3& point2, D3DXVECTOR3& point3)
	{
		return PlaneByThreeDots(point1.x, point1.y, point1.z,
			point2.x, point2.y, point2.z,
			point3.x, point3.y, point3.z);
	}
}

#endif//__PlanesH__