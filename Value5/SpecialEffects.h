/************************************************************************************
����:  SpecialEffects.h
�����: ������� �������

�������� �������, ����������� ����������� �������, ����� ��� ��������� �����.
************************************************************************************/

#ifndef __SpecialEffectsH__
#define __SpecialEffectsH__

#include "Base.h"
#include "EffectHelper.h"
#include "SimpleGraphicsObject.h"

using namespace GraphicsSystem;
using namespace ServiceTools;

namespace GraphicsSystem
{
	__forceinline void ShadowCast(SimpleGraphicsObject* p_object, D3DXPLANE* p_plane)
	{
		PointLightSource<D3DXVECTOR4, D3DXVECTOR3> pointLightSource = BASE.getPointLightSource();
		DirectionalLightSource<D3DXVECTOR4, D3DXVECTOR3> directionalLightSource = BASE.getDirectionalLightSource();
		ID3DX11EffectTechnique* pTechnique = p_object->m_pEffect->GetTechniqueByName("Shadowing");
		
		if (pointLightSource.m_intensity.x != 0.0f ||
			pointLightSource.m_intensity.y != 0.0f ||
			pointLightSource.m_intensity.z != 0.0f)
		{
			D3DXMATRIX shadowMatrix;
			D3DXVECTOR4 pointLightSourcePosition = { pointLightSource.m_position, 1.0f };
			D3DXMatrixShadow(&shadowMatrix, &pointLightSourcePosition, p_plane);
			D3DXMATRIX worldMatrix = p_object->m_worldMatrix * shadowMatrix;
			UpdateEffectBase(&worldMatrix, 0, 0, 0, p_object->m_pEffect);

			ID3DX11EffectTechnique* pOldTechnique = p_object->m_pTechnique;
			p_object->m_pTechnique = pTechnique;
			
			p_object->draw();
			
			p_object->m_pTechnique = pOldTechnique;
			
		}

		if (directionalLightSource.m_intensity.x != 0.0f ||
			directionalLightSource.m_intensity.y != 0.0f ||
			directionalLightSource.m_intensity.z != 0.0f)
		{			

			D3DXMATRIX shadowMatrix;
			D3DXVECTOR4 directionToDirLightSource = { -directionalLightSource.m_direction, 0.0f };
			D3DXMatrixShadow(&shadowMatrix, &directionToDirLightSource, p_plane);
			D3DXMATRIX worldMatrix = p_object->m_worldMatrix * shadowMatrix;
			UpdateEffectBase(&worldMatrix, 0, 0, 0, p_object->m_pEffect);

			ID3DX11EffectTechnique* pOldTechnique = p_object->m_pTechnique;
			p_object->m_pTechnique = pTechnique;

			p_object->draw();

			p_object->m_pTechnique = pOldTechnique;
		}

		ReleaseCOM(pTechnique);
	}
}

#endif//__SpecialEffectsH__